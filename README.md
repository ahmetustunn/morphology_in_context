### Multi-Team: A Multi-attention, Multi-decoder Approach to Morphological Analysis. ###
This is the code to reproduce the results of the paper ``Multi-Team: A Multi-attention, Multi-decoder Approach to Morphological Analysis.''

### Reproduce ###
To re-run all our experiments, run the following:
```
git clone https://robvanderg@bitbucket.org/ahmetustunn/morphology_in_context.git
cd morphology_in_context
git clone https://github.com/sigmorphon/2019.git
cd 2019
git reset 75dbb844dec88dab54f8f53e8a71d49f70f07532
cd ..
cp 2019/task2/* data/
./scripts/runAll.sh
```

To generate all the tables and graphs from the paper, run:
```
./scripts/genAll.sh
```


### Use the tagger ###
To see all options, run:
```
>> python3 src/tagger.py --help
Usage: tagger.py [options]

Contextual Morpheme Tagger

Options:
  -h, --help            show this help message and exit
  --train=TRAIN_FILE    path to train file
  --dev=DEV_FILE        path to dev file
  --test=TEST_FILE      path to test file
  --dir=DIR             process a whole directory
  --out=OUT             write prediction, scores will be written to stdout
  --model=MODEL_FILE    path to model, when --train is used will be written,
                        otherwise will be loaded
  --train_size=TRAIN_SIZE
                        maximum number of words to use for training(0 means no
                        limit)
  --predict=PREDICT     List of the files to be predicted
  --seed=SEED           seed for intialization as well as shuffling of data
  --noshuffle           Shuffle training data
  --epoch=EPOCH         Number of epochs
  --tf_ratio=TF_RATIO   Teacher forcing ratio
  --dropout=DROPOUT     encoder dropout
  --patience=PATIENCE   Patience
  --morph_dec_model_type=MORPH_DEC_MODEL_TYPE
                        morpheme decoder model type ['char-hidden', 'context-
                        concat', 'context-hidden']
  --lemma_dec_model_type=LEMMA_DEC_MODEL_TYPE
                        lemma decoder model type ['char-hidden', 'context-
                        concat', 'context-hidden']
  --morph_dec_attn_type=MORPH_DEC_ATTN_TYPE
                        decoder rnn type ['char', 'char-context']
  --lemma_dec_attn_type=LEMMA_DEC_ATTN_TYPE
                        decoder rnn type ['char', 'char-context']
  --enc_char_emb_size=ENC_CHAR_EMB_SIZE
                        Character embedding size for encoder
  --enc_wordform_emb_size=ENC_WORDFORM_EMB_SIZE
                        Wordform embedding size for encoder
  --enc_tag_emb_size=ENC_TAG_EMB_SIZE
                        Morpheme tag embedding size for tag encoder
  --enc_char_hidden_size=ENC_CHAR_HIDDEN_SIZE
                        Hidden size for character encoder
  --enc_word_hidden_size=ENC_WORD_HIDDEN_SIZE
                        Hidden size for sentence encoder
  --enc_rnn_type=ENC_RNN_TYPE
                        encoder rnn type ['gru', 'lstm']
  --context_concat_size=CONTEXT_CONCAT_SIZE
                        Context embedding size for context-concat
  --treebank_emb_size=TREEBANK_EMB_SIZE
                        Treebank embedding size for treebank sensitivity
  --enc_wordform_type=ENC_WORDFORM_TYPE
                        encoder wordform type type ['embed', 'pretrained',
                        'encode']
  --enc_char_bidim      Enable bidirectional character encoding
  --dec_emb_size=DEC_EMB_SIZE
                        Output embedding size for decoder
  --dec_hidden_size=DEC_HIDDEN_SIZE
                        Hidden size for decoder
  --dec_rnn_type=DEC_RNN_TYPE
                        decoder rnn type ['gru', 'lstm']
  --enc_learning_rate=ENC_LEARNING_RATE
                        Learning rate
  --dec_learning_rate=DEC_LEARNING_RATE
                        Learning rate
  --learning_rate_step=LEARNING_RATE_STEP
                        Learning rate step
  --morph_lr_ratio=MORPH_LR_RATIO
                        Learning rate step
  --no_joint_grad_step  Learning rate step
```

Normal usage is as follows:

For training:
```
python3 src/tagger.py --model models/embs.UD_Turkish-IMST.elmo --train data/UD_Turkish-IMST/tr_imst-um-train.conllu.elmo --dev data/UD_Turkish-IMST/tr_imst-um-dev.conllu.elmo 
```

For testing:
```
python3 src/tagger.py --model models/embs.UD_Turkish-IMST.elmo  --dev data/UD_Turkish-IMST/tr_imst-um-dev.conllu.elmo 
```
