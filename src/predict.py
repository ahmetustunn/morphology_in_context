import argparse
import pickle
import torch
import os
import logging
import sys

from tqdm import tqdm
from data import Sentence, read_conllu_dataset
from model import Encoder, TagEncoder, CharAttnDecoder


LOG_FORMAT = '%(asctime)s, %(levelname)-8s %(message)s'

LOGGER = logging.getLogger('MorphTagger')
logging.basicConfig(format=LOG_FORMAT, datefmt='%d-%m-%Y:%H:%M:%S', level=logging.INFO, stream=sys.stdout)


def predict_sentence(sentence, encoder, tag_encoder, decoder_lemma, decoder_morph_tags, dataset, device=torch.device("cpu"),
                     max_lemma_len=20, max_morph_features_len=10):

    if len(sentence.wordforms) == 0:
        return ""

    max_token_len = max([len(surface) for surface in sentence.wordforms])+2

    encoded_surfaces = torch.zeros((len(sentence.wordforms), max_token_len), dtype=torch.long)
    seq_lens_surface = torch.LongTensor(len(sentence.wordforms))
    for ix, surface in enumerate(sentence.wordforms):
        encoded_surface = dataset.encode(surface, dataset.surface_char2id, add_start_tag=True)
        seq_lens_surface[ix] = encoded_surface.size()[0]
        encoded_surfaces[ix, :encoded_surface.size()[0]] = encoded_surface

    if not len(sentence.embs) > 0:
        x_word = dataset.encode(sentence.wordforms, dataset.surface_word2id, add_start_tag=False, add_end_tag=False)
    else:
        embs = []
        for emb in sentence.embs:
            embs.append(emb)
            x_word = torch.Tensor(embs)

    if len(sentence.treebanks) > 0:
        x_treebank = dataset.encode(sentence.treebanks, dataset.treebank2id, add_start_tag=False, add_end_tag=False)
    else:
        x_treebank = []

    encoded_surfaces = encoded_surfaces.to(device)
    seq_lens_surface = seq_lens_surface.to(device)
    x_word = x_word.to(device)

    x_char = encoded_surfaces.view(1, *encoded_surfaces.size())
    x_char_seq = seq_lens_surface.view(1, *seq_lens_surface.shape)
    x_word = x_word.view(1, *x_word.shape)

    if torch.is_tensor(x_treebank):
        x_treebank = x_treebank.to(device)
        x_treebank = x_treebank.view(1, *x_treebank.shape)

    # Run encoder
    word_representations, char_enc_out, context_aware_representations = encoder((x_char, x_char_seq), x_word, x_treebank)

    # Run morph features decoder for each word
    morph_features = []
    words_count = context_aware_representations.size(0)
    for i in range(words_count):
        morph_feature = decoder_morph_tags.predict(char_enc_out, x_char_seq,
                                                   word_representations, context_aware_representations,
                                                   None,
                                                   i,
                                                   max_len=max_morph_features_len, device=device)
        morph_features.append(';'.join(morph_feature))

    x_tag = torch.zeros((len(morph_features), len(dataset.morph_tag2id) - 4), dtype=torch.long)
    for i, tag_seq in enumerate(morph_features):
        if not tag_seq == '':
            tag = dataset.convert_tag_seq(tag_seq.split(';'), dataset.morph_tag2id)
            x_tag[i] = tag

    x_tag = x_tag.to(device)
    tag_emb = tag_encoder(x_tag)

    # Run lemma decoder for each word
    lemmas = []
    for i in range(words_count):
        lemma = decoder_lemma.predict(char_enc_out, x_char_seq,
                                      word_representations, context_aware_representations,
                                      tag_emb,
                                      i,
                                      max_len=max_lemma_len, device=device)
        lemmas.append(''.join(lemma))



    conll_sentence = "# Sentence\n"
    for i, (surface, lemma, morph_feature) in enumerate(zip(sentence.wordforms, lemmas, morph_features)):
        conll_sentence += "{}\t{}\t{}\t_\t_\t{}\t_\t_\t_\t_\n".format(i+1, surface, lemma, morph_feature)
    return conll_sentence


def predict(dev_file, model_file, out_file):
    # load params, TODO probably does not work like this?
    seed, shuffle, epoch, tf_ratio, dropout, patience, morph_dec_model_type, lemma_dec_model_type, morph_dec_attn_type, lemma_dec_attn_type, enc_char_emb_size, enc_wordform_emb_size, enc_tag_emb_size, enc_char_hidden_size, enc_word_hidden_size, enc_rnn_type, enc_wordform_type, char_bidi, context_concat_size, dec_emb_size, dec_hidden_size, dec_rnn_type, enc_learning_rate, dec_learning_rate, learning_rate_step, morph_lr_ratio, joint_grad_step, max_sent_len, max_word_len, treebank_emb_size = pickle.load(open(os.path.join(model_file, 'params'), 'rb'))
    
    LOG_FORMAT = '%(asctime)s, %(levelname)-8s %(message)s'

    LOGGER = logging.getLogger('MorphTagger')
    logging.basicConfig(format=LOG_FORMAT, datefmt='%d-%m-%Y:%H:%M:%S', level=logging.INFO, stream=sys.stdout)

    # Select cuda as device if available
    device = torch.device('cuda' if torch.cuda.is_available() else "cpu")
    LOGGER.info("Using {} as default device".format(device))


    # LOAD DATASET
    LOGGER.info('Loading dataset...')
    train_set = pickle.load(open(os.path.join(model_file, 'train_set'), 'rb'))
    data_sentences = read_conllu_dataset(dev_file)

    # Treebank embeddings check
    len_treebank_vocab = None
    if treebank_emb_size > 0:
        len_treebank_vocab = len(train_set.treebank2id)

    # LOAD ENCODER MODEL
    LOGGER.info('Loading Encoder...')
    encoder = Encoder(enc_char_emb_size, enc_wordform_emb_size, enc_char_hidden_size, enc_word_hidden_size,
                            len(train_set.surface_char2id), len(train_set.surface_word2id),
                      treebank_emb_size=treebank_emb_size, treebank_vocab_size=len_treebank_vocab,
                      bi_char=char_bidi,
                      rnn_type=enc_rnn_type, wordform_type=enc_wordform_type,
                      dropout_ratio=dropout, device=device)
    encoder.load_state_dict(torch.load(os.path.join(model_file, 'encoder'), map_location=device))
    encoder = encoder.to(device)

    # LOAD TAG ENCODER MODEL
    LOGGER.info('Loading Tag Encoder...')
    tag_encoder = TagEncoder(enc_tag_emb_size, len(train_set.morph_tag2id)-4)
    tag_encoder.load_state_dict(torch.load(os.path.join(model_file, 'tag_encoder'), map_location=device))
    tag_encoder = tag_encoder.to(device)

    # LOAD MORPH DECODER MODEL
    LOGGER.info('Loading Morph Decoder...')
    decoder_morph_tags = CharAttnDecoder('tag', dec_emb_size, -1, dec_hidden_size,
                                            train_set.morph_tag2id,
                                         max_word_len=max_word_len, max_sent_len=max_sent_len,
                                         context_concat_size=context_concat_size,
                                         bi_char=char_bidi,
                                         rnn_type=enc_rnn_type, model_type=morph_dec_model_type, attn_type=morph_dec_attn_type,
                                    dropout_ratio=dropout, device=device)
    decoder_morph_tags.load_state_dict(torch.load(os.path.join(model_file, 'decoder_morph'), map_location=device))
    decoder_morph_tags = decoder_morph_tags.to(device)

    # LOAD LEMMA DECODER MODEL
    LOGGER.info('Loading Lemma Decoder...')
    decoder_lemma = CharAttnDecoder('lemma', dec_emb_size, enc_tag_emb_size, dec_hidden_size,
                                    train_set.lemma_char2id,
                                    max_word_len=max_word_len, max_sent_len=max_sent_len,
                                    context_concat_size=context_concat_size,
                                    bi_char=char_bidi,
                                    rnn_type=enc_rnn_type, model_type=lemma_dec_model_type, attn_type=lemma_dec_attn_type,
                                    dropout_ratio=dropout, device=device)
    decoder_lemma.load_state_dict(torch.load(os.path.join(model_file, 'decoder_lemma'), map_location=device))
    decoder_lemma = decoder_lemma.to(device)

    encoder.eval()
    tag_encoder.eval()
    decoder_lemma.eval()
    decoder_morph_tags.eval()

    # Make predictions and save to file
    with open(out_file, 'w', encoding='UTF-8') as f:
        for sentence in tqdm(data_sentences):
            # if len(sentence) > max_words:
            #    continue
            conllu_sentence = predict_sentence(sentence, encoder, tag_encoder, decoder_lemma, decoder_morph_tags,
                                                train_set, device=device)
            f.write(conllu_sentence)
            f.write('\n')

