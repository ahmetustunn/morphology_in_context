import random
import utils
from itertools import chain
from collections import Counter
from conllu import parse_incr

import torch
from torch.utils.data import Dataset


class Sentence(object):

    def __init__(self, tokenlist):

        self.wordforms = []
        self.lemmas = []
        self.msds = []
        self.embs = []
        self.treebanks = []

        for token in tokenlist:
            self.wordforms.append(token['form'])
            self.lemmas.append(token['lemma'])
            #TODO fix in a more general way
            if token['feats'] is not None:
                self.msds.append(list(token['feats'])[0].split(';'))
                #self.msds.append(token['feats'].split(';'))
            else:
                self.msds.append(['_'])
            if token['misc'] is not None and 'emb' in token['misc']:
                self.embs.append([])
                for x in token['misc']['emb'].split(','):
                    self.embs[-1].append(float(x))
            if token['misc'] is not None and 'treebank' in token['misc']:
                self.treebanks.append(token['misc']['treebank'])

    def get_tags_as_str(self):
        return [';'.join(msd) for msd in self.msds]

    def __len__(self):
        return len(self.lemmas)


def read_conllu_dataset(conllu_file):
    sentences = []
    data_file = open(conllu_file, "r", encoding="utf-8")
    for tokelist in parse_incr(data_file):
        s = Sentence(tokelist)
        sentences.append(s)

    return sentences


def read_conllu_surface(conllu_file):
    sentences = []
    data_file = open(conllu_file, "r", encoding="utf-8")
    for tokelist in parse_incr(data_file):
        s = Sentence(tokelist)
        sentences.append(s.wordforms)

    return sentences


class ConlluDataset(Dataset):

    PAD_token = '<p>'
    EOS_token = '<e>'
    START_TAG = '<s>'
    UNK_token = '<u>'

    def __init__(self, conllu_train_file, conllu_dev_file=None, surface_word2id=None, surface_word2count=None, surface_char2id=None, lemma_char2id=None, morph_tag2id=None, treebank2id=None, mode='train', word_drop_rate=0, max_sentences=0, max_words=0):

        self.sentences = read_conllu_dataset(conllu_train_file)
        self.word_drop_rate = word_drop_rate
        self.dev_sentences = None
        if conllu_dev_file:
            self.dev_sentences = read_conllu_dataset(conllu_dev_file)

        if 0 < max_sentences < len(self.sentences):
            self.sentences = self.sentences[:max_sentences]
        if max_words > 0:
            wordCounter = 0
            for sentIdx in range(len(self.sentences)):
                wordCounter += len(self.sentences[sentIdx])
                if wordCounter > max_words:
                    break
            self.sentences = self.sentences[:sentIdx]
        if surface_char2id:
            self.surface_char2id = surface_char2id
        else:
            self.surface_char2id = dict()
            self.surface_char2id[self.PAD_token] = len(self.surface_char2id)
            self.surface_char2id[self.EOS_token] = len(self.surface_char2id)
            self.surface_char2id[self.START_TAG] = len(self.surface_char2id)
            self.surface_char2id[self.UNK_token] = len(self.surface_char2id)
        if surface_word2id:
            self.surface_word2id = surface_word2id
            self.surface_word2count = surface_word2count
        else:
            self.surface_word2count = dict()
            self.surface_word2id = dict()
            self.surface_word2id[self.PAD_token] = len(self.surface_word2id)
            self.surface_word2id[self.UNK_token] = len(self.surface_word2id)
        if lemma_char2id:
            self.lemma_char2id = lemma_char2id
        else:
            self.lemma_char2id = dict()
            self.lemma_char2id[self.PAD_token] = len(self.lemma_char2id)
            self.lemma_char2id[self.EOS_token] = len(self.lemma_char2id)
            self.lemma_char2id[self.START_TAG] = len(self.lemma_char2id)
            self.lemma_char2id[self.UNK_token] = len(self.lemma_char2id)
        if morph_tag2id:
            self.morph_tag2id = morph_tag2id
            self.morph_id2tag = self.switch_val2key(morph_tag2id)
        else:
            self.morph_id2tag = dict()
            self.morph_tag2id = dict()
            self.morph_tag2id[self.PAD_token] = len(self.morph_tag2id)
            self.morph_tag2id[self.EOS_token] = len(self.morph_tag2id)
            self.morph_tag2id[self.START_TAG] = len(self.morph_tag2id)
            self.morph_tag2id[self.UNK_token] = len(self.morph_tag2id)
        if treebank2id:
            self.treebank2id = treebank2id
        else:
            self.treebank2id = dict()
            self.treebank2id[self.PAD_token] = len(self.treebank2id)
        self.mode = mode
        if mode == 'train':
            self.create_vocabs()

    def get_max_sent_length(self):
        # get max sentence length for sentence level context attention
        return max([len(sentence) for sentence in self.sentences])

    def get_max_word_length(self):
        # get max word length for word level character attention
        unique_words = set(chain(*[sentence.wordforms for sentence in self.sentences]))
        return len(max(unique_words, key=len))+2

    def get_word_count(self):
        # get max word length for word level character attention
        w_count = Counter(chain(*[sentence.wordforms for sentence in self.sentences]))
        return w_count

    def create_vocabs(self):
        # Update surface_word2id
        self.surface_word2count = Counter(chain(*[sentence.wordforms for sentence in self.sentences]))
        unique_words = set(self.surface_word2count)
        for w in unique_words:
            self.surface_word2id[w] = len(self.surface_word2id)

        # Update surface_char2id
        unique_words = set(chain(*[sentence.wordforms for sentence in self.sentences]))
        unique_chars = set(chain(*[word for word in unique_words]))
        for ch in unique_chars:
            self.surface_char2id[ch] = len(self.surface_char2id)

        # Update lemma_char2id
        unique_lemmas = set(chain(*[sentence.lemmas for sentence in self.sentences]))
        unique_chars = set(chain(*[lemma for lemma in unique_lemmas]))
        for ch in unique_chars:
            self.lemma_char2id[ch] = len(self.lemma_char2id)

        # Update morph_tag2id and morph_id2tag
        unique_msds = list(chain(*[sentence.msds for sentence in self.sentences]))
        unique_tags = set(chain(*[msd for msd in unique_msds]))
        for tag in unique_tags:
            self.morph_tag2id[tag] = len(self.morph_tag2id)

        if self.dev_sentences:
            dev_unique_msds = list(chain(*[sentence.msds for sentence in self.dev_sentences]))
            dev_unique_tags = set(chain(*[msd for msd in dev_unique_msds]))
            for tag in dev_unique_tags:
                if tag not in self.morph_tag2id:
                    self.morph_tag2id[tag] = len(self.morph_tag2id)

        self.morph_id2tag = self.switch_val2key(self.morph_tag2id)

        tbs = set(chain(*[sentence.treebanks for sentence in self.sentences]))
        for t in tbs:
            self.treebank2id[t] = len(self.treebank2id)

    # https://github.com/bplank/bilstm-aux/blob/master/src/structbilty.py
    def drop_word(self, w):
        """
        drop x if x is less frequent (cf. Kiperwasser & Goldberg, 2016)
        """
        return random.random() > (self.surface_word2count.get(w) / (self.word_drop_rate + self.surface_word2count.get(w)))

    def _encode(self, seq, vocab, add_end_tag=True, add_start_tag=False):
        res = []
        if add_start_tag:
            res.append(vocab[ConlluDataset.START_TAG])
        for token in seq:
            if token in vocab:
                if self.drop_word(token) and self.mode == 'train':
                    res.append(vocab[ConlluDataset.UNK_token])
                else:
                    res.append(vocab[token])
            else:
                res.append(vocab[ConlluDataset.UNK_token])
        if add_end_tag:
            res.append(vocab[ConlluDataset.EOS_token])
        return torch.tensor(res, dtype=torch.long)

    @staticmethod
    def switch_val2key(dictionary):
        res = dict()
        for key, val in dictionary.items():
            res[val] = key
        return res

    @staticmethod
    def encode(seq, vocab, add_end_tag=True, add_start_tag=False):
        res = []
        if add_start_tag:
            res.append(vocab[ConlluDataset.START_TAG])
        for token in seq:
            if token in vocab:
                res.append(vocab[token])
            else:
                res.append(vocab[ConlluDataset.UNK_token])
        if add_end_tag:
            res.append(vocab[ConlluDataset.EOS_token])
        return torch.tensor(res, dtype=torch.long)

    @staticmethod
    def convert_tag_seq(tag_seq, vocab):
        res = torch.zeros((1, len(vocab)-4), dtype=torch.long)
        for tag in tag_seq:
            if tag == ConlluDataset.START_TAG or tag == ConlluDataset.EOS_token or tag == ConlluDataset.PAD_token or tag == ConlluDataset.UNK_token:
                continue
            # Remove id for START, END and UNK mark
            id = vocab[tag] - 3
            res[0, id-1] = id
        return res

    def __len__(self):
        return len(self.sentences)

    def __getitem__(self, index):
        sentence = self.sentences[index]
        max_token_len = max([len(word) + 2 for word in sentence.wordforms])
        max_lemma_len = max([len(lemma) + 1 for lemma in sentence.lemmas])
        max_morph_tags_len = max([len(morph_tag) + 1 for morph_tag in sentence.msds])

        # (Encode char surfaces, Sequence Lengths)
        encoded_surfaces = torch.zeros((len(sentence), max_token_len), dtype=torch.long)
        seq_lens_surface = torch.LongTensor(len(sentence))
        for ix, surface in enumerate(sentence.wordforms):
            encoded_surface = self.encode(surface, self.surface_char2id, add_start_tag=True)
            seq_lens_surface[ix] = encoded_surface.size()[0]
            encoded_surfaces[ix, :encoded_surface.size()[0]] = encoded_surface

        # Encode word surfaces
        if not len(sentence.embs) > 0:
            encoded_word_surface = self._encode(sentence.wordforms, self.surface_word2id, add_start_tag=False, add_end_tag=False)
        else:
            embs = []
            for emb in sentence.embs:
                embs.append(emb)
            encoded_word_surface = torch.Tensor(embs)

        # Encode treebank
        if len(sentence.treebanks) > 0:
            encoded_treebank = self.encode(sentence.treebanks, self.treebank2id, add_start_tag=False, add_end_tag=False)
        else:
            encoded_treebank = []

        # Encode lemmas target
        encoded_lemmas_target = torch.zeros((len(sentence), max_lemma_len), dtype=torch.long)
        seq_lens_lemma_target = torch.LongTensor(len(sentence))
        for ix, lemma in enumerate(sentence.lemmas):
            encoded_lemma = self.encode(lemma, self.lemma_char2id, add_start_tag=False)
            seq_lens_lemma_target[ix] = encoded_lemma.size()[0]
            encoded_lemmas_target[ix, :encoded_lemma.size()[0]] = encoded_lemma

        # Encode surfaces_target
        encoded_morph_tags_target = torch.zeros((len(sentence), max_morph_tags_len), dtype=torch.long)
        seq_lens_tags_target = torch.LongTensor(len(sentence))

        encoded_morph_tags_input = torch.zeros((len(sentence), len(self.morph_tag2id)-4), dtype=torch.long)

        for ix, morph_tag in enumerate(sentence.msds):
            encoded_morph_tag = self.encode(morph_tag, self.morph_tag2id, add_start_tag=False)
            seq_lens_tags_target[ix] = encoded_morph_tag.size()[0]
            encoded_morph_tags_target[ix, :encoded_morph_tag.size()[0]] = encoded_morph_tag

            converted_morph_tag = self.convert_tag_seq(morph_tag, self.morph_tag2id)
            encoded_morph_tags_input[ix] = converted_morph_tag

        encoded_lemma = encoded_lemmas_target
        seq_lens_lemma = seq_lens_lemma_target

        encoded_morph_tag = encoded_morph_tags_target
        seq_lens_tags = seq_lens_tags_target

        input_morph_tag = encoded_morph_tags_input

        return (encoded_surfaces, seq_lens_surface), encoded_word_surface, input_morph_tag, encoded_treebank, (encoded_lemma, seq_lens_lemma), (encoded_morph_tag, seq_lens_tags)
