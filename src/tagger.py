from optparse import OptionParser
import os
import train
import predict
import eval

import random
import torch
import numpy as np

def err(msg):
    print('Error: ' + msg)
    exit(0)

if __name__ == '__main__':
    parser = OptionParser(description='Contextual Morpheme Tagger')
    
    parser.add_option("--train", dest="train_file", help="path to train file")
    parser.add_option("--dev", dest="dev_file", help="path to dev file")
    parser.add_option("--test", dest="test_file", help="path to test file")
    parser.add_option("--dir", dest="dir", help="process a whole directory")
    parser.add_option("--out", dest="out", help="write prediction, scores will be written to stdout")
    parser.add_option("--model", dest="model_file", help="path to model, when --train is used will be written, otherwise will be loaded")
    #TODO rename to modelDir
    parser.add_option('--train_size', type=int, default=0,dest='train_size', help="maximum number of words to use for training(0 means no limit)")
    parser.add_option('--predict', action='append', help="List of the files to be predicted")

    #TODO use seed
    parser.add_option("--seed", type=int, default=8446, help="seed for intialization as well as shuffling of data")
    parser.add_option("--noshuffle", action="store_false", dest="shuffle", default=True, help="Shuffle training data")
    parser.add_option("--epoch", type=int, default=100, help="Number of epochs")
    parser.add_option("--tf_ratio", type=float, default=0.5, help="Teacher forcing ratio")
    parser.add_option("--dropout", type=float, default=0.25, help="encoder dropout")
    parser.add_option("--patience", type=int, default=4, help="Patience")

    parser.add_option("--morph_dec_model_type", type=str, default='char-hidden', help="morpheme decoder model type ['char-hidden', 'context-concat', 'context-hidden']")
    parser.add_option("--lemma_dec_model_type", type=str, default='char-hidden', help="lemma decoder model type ['char-hidden', 'context-concat', 'context-hidden']")
    parser.add_option("--morph_dec_attn_type", type=str, default='char-context', help="decoder rnn type ['char', 'char-context']")
    parser.add_option("--lemma_dec_attn_type", type=str, default='char', help="decoder rnn type ['char', 'char-context']")

    parser.add_option("--enc_char_emb_size", type=int, default=128, help="Character embedding size for encoder")
    parser.add_option("--enc_wordform_emb_size", type=int, default=256, help="Wordform embedding size for encoder")
    parser.add_option("--enc_tag_emb_size", type=int, default=32, help="Morpheme tag embedding size for tag encoder")
    parser.add_option("--enc_char_hidden_size", type=int, default=1024, help="Hidden size for character encoder")
    parser.add_option("--enc_word_hidden_size", type=int, default=1024, help="Hidden size for sentence encoder")
    parser.add_option("--enc_rnn_type", type=str, default='gru', help="encoder rnn type ['gru', 'lstm']")
    parser.add_option("--context_concat_size", type=int, default=-1, help="Context embedding size for context-concat")
    parser.add_option("--treebank_emb_size", type=int, default=0, help="Treebank embedding size for treebank sensitivity")
    parser.add_option("--enc_wordform_type", type=str, default='embed', help="encoder wordform type type ['embed', 'pretrained', 'encode']")
    parser.add_option("--enc_char_bidim", dest='enc_char_bidim', action='store_true', default=False, help="Enable bidirectional character encoding")

    parser.add_option("--dec_emb_size", type=int, default=128, help="Output embedding size for decoder")
    parser.add_option("--dec_hidden_size", type=int, default=1024, help="Hidden size for decoder")
    parser.add_option("--dec_rnn_type", type=str, default='gru', help="decoder rnn type ['gru', 'lstm']")
    #TODO is dec_rnn_type used?

    parser.add_option("--enc_learning_rate", type=float, default=0.0002, help="Learning rate")
    parser.add_option("--dec_learning_rate", type=float, default=0.0002, help="Learning rate")
    parser.add_option("--learning_rate_step", type=int, default=6, help="Learning rate step")
    parser.add_option("--morph_lr_ratio", type=float, default=1.0, help="Learning rate step")
    parser.add_option("--no_joint_grad_step", dest='joint_grad_step', action="store_false", default=True, help="Learning rate step")

    (opts, args) = parser.parse_args()

    # Run on whole directory at once
    # This is old way of running, but might still be very useful for running on whole UD
    if opts.dir != None:
        err('the --dir option still has to be implemented, sorry')
        #if opts.train_file != None:
        #    train.trainDir(opts.dir)
        #if opts.dev_file != None:
        #    predict.predictDir('data.rob/tr.poly/', 'morphnet', 'tr_pud-um-dev.conllu.poly')
        #    eval_results = eval.evaluateDir('Turkish-PUD', 'data.rob/tr.poly/', model_name='morphnet')

    if opts.model_file == None:
        err('please specify path to model by using --model')

    if opts.train_file == None and opts.dev_file == None and opts.predict == None:
        err('please specify either training or development data')

    if opts.train_file == None and not os.path.exists(opts.model_file):
        err('No training file specified, and ' + opts.model_file + ' does not exists')

    if opts.test_file == None:
         opts.test_file = opts.dev_file

    if opts.train_file != None:
        #TODO can we run without dev?
        train.train(opts.train_file, opts.dev_file, opts.test_file, opts.model_file, opts.seed, opts.shuffle, opts.epoch, opts.tf_ratio, opts.dropout, opts.patience, opts.morph_dec_model_type, opts.lemma_dec_model_type, opts.morph_dec_attn_type, opts.lemma_dec_attn_type, opts.enc_char_emb_size, opts.enc_wordform_emb_size, opts.enc_tag_emb_size, opts.enc_char_hidden_size, opts.enc_word_hidden_size, opts.enc_rnn_type, opts.enc_wordform_type, opts.enc_char_bidim,
                    opts.context_concat_size, opts.dec_emb_size, opts.dec_hidden_size, opts.dec_rnn_type, opts.enc_learning_rate, opts.dec_learning_rate, opts.learning_rate_step, opts.morph_lr_ratio, opts.joint_grad_step, opts.train_size, opts.treebank_emb_size)


    if opts.predict != None:
        for i in opts.predict:
            predict.predict(i, opts.model_file, os.path.join(opts.model_file, os.path.split(i)[1]+'.output'))
            # eval_results = eval.evaluate(opts.dev_file, opts.out)
            # for k, v in eval_results.items():
            #    print('{}: {}'.format(k, v))

    random.seed(opts.seed)
    np.random.seed(opts.seed)
    torch.manual_seed(opts.seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(opts.seed)

