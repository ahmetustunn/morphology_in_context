import os
import pickle
import argparse
import random

import torch
import torch.nn as nn
from torch.optim.lr_scheduler import MultiStepLR
from torch.utils.data import DataLoader
from tqdm import tqdm

from data import ConlluDataset
from eval import evaluate
from model import Encoder, TagEncoder, CharAttnDecoder
from utils import masked_cross_entropy

import logging
import sys

def lr_decay_step(lr, model, factor=0.1, weight_decay=0.0):
    lr *= factor
    optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)
    return lr, optimizer

def step_optim(optims):
    for o in optims:
        o.step()

def train(train_file, dev_file, test_file, model_file, seed, shuffle, epoch, tf_ratio, dropout, patience, morph_dec_model_type, lemma_dec_model_type, morph_dec_attn_type, lemma_dec_attn_type, enc_char_emb_size, enc_wordform_emb_size, enc_tag_emb_size, enc_char_hidden_size, enc_word_hidden_size, enc_rnn_type, enc_wordform_type, char_bidi, context_concat_size, dec_emb_size, dec_hidden_size, dec_rnn_type, enc_learning_rate, dec_learning_rate, learning_rate_step, morph_lr_ratio, joint_grad_step, train_size, treebank_emb_size):
    LOG_FORMAT = '%(asctime)s, %(levelname)-8s %(message)s'

    LOGGER = logging.getLogger('MorphTagger')
    logging.basicConfig(format=LOG_FORMAT, datefmt='%d-%m-%Y:%H:%M:%S', level=logging.INFO, stream=sys.stdout)

    # Select cuda as device if available
    device = torch.device('cuda' if torch.cuda.is_available() else "cpu")
    LOGGER.info("Using {} as default device".format(device))
    
    # Load train set
    train_set = ConlluDataset(train_file, conllu_dev_file=dev_file if train_size != 0 else None, word_drop_rate=dropout, max_words=train_size)
    train_loader = DataLoader(train_set, shuffle=shuffle)

    # Load validation data
    val_set = ConlluDataset(dev_file, surface_word2id=train_set.surface_word2id, 
                        surface_word2count=train_set.surface_word2count, surface_char2id=train_set.surface_char2id,
                        lemma_char2id=train_set.lemma_char2id, morph_tag2id=train_set.morph_tag2id, treebank2id=train_set.treebank2id, mode='test')
    val_loader = DataLoader(val_set)

    # Get max length of sentences and words in test data for attention
    test_set = ConlluDataset(test_file, surface_word2id=train_set.surface_word2id,
                        surface_word2count=train_set.surface_word2count, surface_char2id=train_set.surface_char2id,
                        lemma_char2id=train_set.lemma_char2id, morph_tag2id=train_set.morph_tag2id, treebank2id=train_set.treebank2id, mode='test')


    # TODO should consider also test data
    max_sent_len = max([train_set.get_max_sent_length(), val_set.get_max_sent_length(), test_set.get_max_sent_length()])
    max_word_len = max([train_set.get_max_word_length(), val_set.get_max_word_length(), test_set.get_max_word_length()])

    if not os.path.exists(model_file):
        os.makedirs(model_file)
    LOGGER.info("Saving models in {}".format(model_file))

    # Treebank embeddings check
    len_treebank_vocab = None
    if treebank_emb_size > 0:
        len_treebank_vocab = len(train_set.treebank2id)

    # Build Models
    # Initialize encoder and decoders
    LOGGER.info('Building models for data: {}'.format(train_file))
    encoder = Encoder(enc_char_emb_size, enc_wordform_emb_size, enc_char_hidden_size, enc_word_hidden_size,
                    len(train_set.surface_char2id), len(train_set.surface_word2id),
                    treebank_emb_size=treebank_emb_size, treebank_vocab_size=len_treebank_vocab,
                    bi_char=char_bidi,
                    wordform_type=enc_wordform_type,
                    dropout_ratio=dropout, device=device)

    encoder = encoder.to(device)

    tag_encoder = TagEncoder(enc_tag_emb_size, len(train_set.morph_tag2id)-4)
    tag_encoder = tag_encoder.to(device)

    decoder_morph_tags = CharAttnDecoder('tag', dec_emb_size, -1, enc_word_hidden_size, train_set.morph_tag2id,
                                         context_concat_size=context_concat_size,
                                         bi_char=char_bidi,
                                        max_word_len=max_word_len, max_sent_len=max_sent_len,
                                        model_type=morph_dec_model_type, attn_type=morph_dec_attn_type,
                                        dropout_ratio=dropout, device=device)
    #TODO decoder + encoder have same dropout!

    decoder_morph_tags = decoder_morph_tags.to(device)

    decoder_lemma = CharAttnDecoder('lemma', dec_emb_size, enc_tag_emb_size, enc_word_hidden_size, train_set.lemma_char2id,
                                    context_concat_size=context_concat_size,
                                    bi_char=char_bidi,
                                    max_word_len=max_word_len, max_sent_len=max_sent_len,
                                    model_type=lemma_dec_model_type, attn_type=lemma_dec_attn_type,
                                    dropout_ratio=dropout, device=device)
    decoder_lemma = decoder_lemma.to(device)

    # Define loss and optimizers
    criterion = nn.NLLLoss().to(device)

    # Create optimizers
    encoder_lr = enc_learning_rate
    tag_encoder_lr = enc_learning_rate
    decoder_lemma_lr = dec_learning_rate
    decoder_morph_lr = dec_learning_rate

    encoder_optimizer = torch.optim.Adam(encoder.parameters(), lr=encoder_lr)
    tag_encoder_optimizer = torch.optim.Adam(tag_encoder.parameters(), lr=tag_encoder_lr)
    decoder_lemma_optimizer = torch.optim.Adam(decoder_lemma.parameters(), lr=decoder_lemma_lr)
    decoder_morph_tags_optimizer = torch.optim.Adam(decoder_morph_tags.parameters(), lr=decoder_morph_lr)

    optimizers = [encoder_optimizer, tag_encoder_optimizer, decoder_morph_tags_optimizer, decoder_lemma_optimizer]

    lr_step = learning_rate_step
    encoder_scheduler = MultiStepLR(encoder_optimizer, milestones=list(range(lr_step, epoch, 1)), gamma=0.5)
    tag_encoder_scheduler = MultiStepLR(tag_encoder_optimizer, milestones=list(range(lr_step, epoch, 1)), gamma=0.5)
    decoder_lemma_scheduler = MultiStepLR(decoder_lemma_optimizer, milestones=list(range(lr_step, epoch, 1)), gamma=0.5)
    decoder_morph_tags_scheduler = MultiStepLR(decoder_morph_tags_optimizer, milestones=list(range(lr_step, epoch, 1)), gamma=0.5)

    prev_val_loss = 1000000
    num_epochs_wo_improvement = 0

    LOGGER.info('Training starts for dataset: {}'.format(train_file))
    # Let the training begin
    for epoch in range(epoch):
        LOGGER.info('Epoch {} starts'.format(epoch))
        total_train_loss = 0.0
        lemma_loss = 0.0
        morph_loss = 0.0
        val_loss = 0.0
        val_lemma_loss = 0.0
        val_morph_loss = 0.0

        # Training part
        encoder.train()
        tag_encoder.train()
        decoder_lemma.train()
        decoder_morph_tags.train()

        # LR Schedule
        encoder_scheduler.step()
        tag_encoder_scheduler.step()
        decoder_lemma_scheduler.step()
        decoder_morph_tags_scheduler.step()

        for x_char, x_word, x_tag, x_treebank, y1, y2 in tqdm(train_loader, desc='Training'):
            # Skip sentences longer than max_words
            if x_char[0].size(1) > max_sent_len:
                continue

            # Clear gradients for each sentence
            encoder.zero_grad()
            decoder_lemma.zero_grad()
            decoder_morph_tags.zero_grad()

            # Send input to the device
            x_char = [a.to(device) for a in x_char]
            x_word = x_word.to(device)
            x_tag = x_tag.squeeze(0).to(device)
            y1 = [b.to(device) for b in y1]
            y2 = [b.to(device) for b in y2]

            if torch.is_tensor(x_treebank):
                x_treebank = x_treebank.to(device)

            # Run encoders
            word_embeddings, char_encoder_output, context_embeddings = encoder(x_char, x_word, x_treebank)
            tag_embeddings = tag_encoder(x_tag)

            # Run decoder for each word
            sentence_loss = 0.0

            use_teacher_forcing = True if random.random() < tf_ratio else False

            for j, (_y, decoder) in enumerate(zip([y2, y1], [decoder_morph_tags, decoder_lemma])):

                word_count = word_embeddings.size(0)

                target_batches = _y[0].squeeze(0)
                target_lengths = _y[1].squeeze(0)
                decoder_outputs = decoder(char_encoder_output, x_char[1],
                                            word_embeddings, context_embeddings,
                                          tag_embeddings,
                                          (target_batches, target_lengths),
                                          use_teacher_forcing)

                sentence_loss += masked_cross_entropy(decoder_outputs.transpose(0, 1).contiguous(),
                                                     target_batches.contiguous(),
                                                     target_lengths, device=device)

                total_train_loss += sentence_loss.item() / (word_count * 2.0)
                if j == 0:
                    morph_loss += sentence_loss.item() / (word_count * 1.0)
                    sentence_loss = sentence_loss * morph_lr_ratio
                    sentence_loss.backward(retain_graph=True)
                else:
                    lemma_loss += sentence_loss.item() / (word_count * 1.0)
                    sentence_loss.backward()

                if not joint_grad_step:
                    # Optimization
                    step_optim(optimizers)

            if joint_grad_step:
                step_optim(optimizers)

        encoder.eval()
        tag_encoder.eval()
        decoder_lemma.eval()
        decoder_morph_tags.eval()
        for x_char, x_word, x_tag, x_treebank, y1, y2 in tqdm(val_loader, desc='Validation'):
            # Skip sentences longer than max_words
            if x_char[0].size(1) > max_sent_len:
                continue

            # Send input to the device
            x_char = [a.to(device) for a in x_char]
            x_word = x_word.to(device)
            x_tag = x_tag.squeeze(0).to(device)
            y1 = [b.to(device) for b in y1]
            y2 = [b.to(device) for b in y2]

            if torch.is_tensor(x_treebank):
                x_treebank = x_treebank.to(device)

            # Run encoders
            word_embeddings, char_encoder_output, context_embeddings = encoder(x_char, x_word, x_treebank)
            tag_embeddings = tag_encoder(x_tag)

            use_teacher_forcing = True if random.random() < tf_ratio else False

            # Run decoder for each word
            sentence_loss = 0.0

            for j, (_y, decoder) in enumerate(zip([y2, y1], [decoder_morph_tags, decoder_lemma])):

                word_count = word_embeddings.size(0)

                target_batches = _y[0].squeeze(0)
                target_lengths = _y[1].squeeze(0)
                decoder_outputs = decoder(char_encoder_output, x_char[1],
                                            word_embeddings, context_embeddings,
                                          tag_embeddings,
                                          (target_batches, target_lengths),
                                          use_teacher_forcing)

                sentence_loss += masked_cross_entropy(decoder_outputs.transpose(0, 1).contiguous(),
                                                     target_batches.contiguous(),
                                                     target_lengths, device=device)

                val_loss += sentence_loss.item() / (word_count * 2.0)
                if j == 0:
                    val_morph_loss += sentence_loss.item() / (word_count * 1.0)
                else:
                    val_lemma_loss += sentence_loss.item() / (word_count * 1.0)

        LOGGER.info('Epoch {0:3d}, Loss: {1:7.3f}, Lemma Loss: {2:7.3f}, Morph Loss: {3:7.3f}'.format(
            epoch,
            (1.0 * total_train_loss) / len(train_loader),
            (1.0 * lemma_loss) / len(train_loader),
            (1.0 * morph_loss) / len(train_loader)
        ))
        LOGGER.info('Val Loss: {1:7.3f}, Val Lemma Loss: {2:7.3f}, Val Morph Loss: {3:7.3f}'.format(
            epoch,
            (1.0 * val_loss) / len(val_loader),
            (1.0 * val_lemma_loss) / len(val_loader),
            (1.0 * val_morph_loss) / len(val_loader)
        ))

        if val_loss >= prev_val_loss:
            num_epochs_wo_improvement += 1
            if num_epochs_wo_improvement >= patience:
                break
        else:
            num_epochs_wo_improvement = 0
            prev_val_loss = val_loss

            # save models
            LOGGER.info('Acc Increased, Saving models...')
            torch.save(encoder.state_dict(),
                        os.path.join(model_file, 'encoder'))
            torch.save(tag_encoder.state_dict(),
                        os.path.join(model_file, 'tag_encoder'))
            torch.save(decoder_lemma.state_dict(),
                        os.path.join(model_file, 'decoder_lemma'))
            torch.save(decoder_morph_tags.state_dict(),
                        os.path.join(model_file, 'decoder_morph'))
            with open(os.path.join(model_file, 'train_set'), 'wb') as f:
                pickle.dump(train_set, f)
            with open(os.path.join(model_file, 'params'), 'wb') as f:
                pickle.dump([seed, shuffle, epoch, tf_ratio, dropout, patience, morph_dec_model_type, lemma_dec_model_type, morph_dec_attn_type, lemma_dec_attn_type, enc_char_emb_size, enc_wordform_emb_size, enc_tag_emb_size, enc_char_hidden_size, enc_word_hidden_size, enc_rnn_type, enc_wordform_type, char_bidi, context_concat_size, dec_emb_size, dec_hidden_size, dec_rnn_type, enc_learning_rate, dec_learning_rate, learning_rate_step, morph_lr_ratio, joint_grad_step, max_sent_len, max_word_len, treebank_emb_size], f)

    # Make predictions and save to file
    LOGGER.info('Training completed')
    LOGGER.info('Evaluation...')
    from predict import predict
    predict(dev_file, model_file, os.path.join(model_file, 'dev.prediction'))
    eval_results = evaluate(dev_file, os.path.join(model_file, 'dev.prediction'))

    if dev_file != test_file:
        predict(test_file, model_file, os.path.join(model_file, 'test.output'))

    LOGGER.info('Evaluation completed')
    writeStr = ''
    for k, v in eval_results.items():
        writeStr += '{}: {}\n'.format(k, v) 
    print(writeStr)
    with open(os.path.join(model_file, 'score'), 'w') as f:
        f.write(writeStr)


