import os
import json

os.system('git clone https://github.com/HIT-SCIR/ELMoForManyLangs.git')
os.system('pip3 install --user torch')

#langs = ['en', 'tr', 'fi', 'zh']
#numbers = ['144', '174', '149', '179']
langs = []
numbers = []
for line in open('scripts/langmapping.txt'):
    tok = line.strip().split(' ')
    langs.append(tok[0])
    numbers.append(tok[-1])

print(langs)
print(numbers)

for lang, number in zip(langs, numbers):
    embDir = 'ELMoForManyLangs/' + lang + '/'
    os.system('wget http://vectors.nlpl.eu/repository/11/' + number + '.zip')
    if not os.path.exists(embDir):
        os.mkdir(embDir)
    os.rename(number + '.zip', embDir + number + '.zip')
    os.system('cd ' + embDir + ' && unzip ' + number + '.zip && rm ' + number + '.zip && cd ../../')
os.system('cp scripts/3.elmo.py ELMoForManyLangs')

for lang in langs:
    data = json.load(open('ELMoForManyLangs/' + lang + '/config.json'))
    data['config_path'] = '../configs/cnn_50_100_512_4096_sample.json'
    json.dump(data, open('ELMoForManyLangs/' + lang + '/config.json', 'w'))

