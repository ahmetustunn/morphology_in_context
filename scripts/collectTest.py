import os
import myutils

tgtDir = 'RUG-02-2'

if not os.path.exists(tgtDir):
    os.mkdir(tgtDir)

for dataset in os.listdir('data'):
    datasetDir = tgtDir + '/' + dataset
    datasetName = myutils.getDatasetName(dataset)
    if not os.path.exists(datasetDir):
        os.mkdir(datasetDir)

    # get scores for this dataset
    ownScores = myutils.getScores('models-test/' + dataset + '/score')
    embScores = myutils.getScores('models-test-emb/' + dataset + '/score')
    treeScores, couple1 = myutils.getTreeScores(dataset, 'models-test-treebank/')
    treeEmbScores, couple2 = myutils.getTreeScores(dataset, 'models-test-treebank-emb.old')

    # avg of Lemma Acc and Morph F1
    ownScore = (ownScores[0] + ownScores[3])/2
    embScore = (embScores[0] + embScores[3])/2
    treeScore = (treeScores[0] + treeScores[3])/2
    treeEmbScore = (treeEmbScores[0] + treeEmbScores[3])/2
    if tgtDir == 'RUG-01-2':
        scores = [ownScore, embScore, treeScore, treeEmbScore]
        names = ['ownScore', 'embScore', 'treeScore', 'treeEmbScore']
    if tgtDir == 'RUG-02-2':
        scores = [ownScore, treeScore]
        names = ['ownScore', 'treeScore']

    # get the best performing method
    highestIdx = scores.index(max(scores))
    highest = names[highestIdx]

    # get the test prediction of the best performing method
    bestTest = ''
    emb = myutils.getEmbType(dataset)
    if highest == 'ownScore':
        bestTest = 'data/' + dataset + '/' + datasetName + '-um-covered-test.conllu.output'
    elif highest == 'embScore':
        bestTest = 'data/' + dataset + '/' + datasetName + '-um-covered-test.conllu.' + emb + '.output'
    elif highest == 'treeScore':
        path1 = 'models-test-treebank/' + dataset + '--' + couple1 + '/' + datasetName + '-um-covered-test.conllu.treebank.output'
        path2 = 'models-test-treebank/' + couple1 + '--' + dataset + '/' + datasetName + '-um-covered-test.conllu.treebank.output'
        if os.path.exists(path1):
            bestTest = path1
        elif os.path.exists(path2):
            bestTest = path2
        else:
            print("NOT FOUND:")
            print(path1)
            print(path2)
            print()
    elif highest == 'treeEmbScore':
        for emb in ['elmo', 'poly', 'fast']:
            path1 = 'models-test-treebank-emb.old/' + dataset + '--' + couple2 + '/' + datasetName + '-um-covered-test.conllu.' + emb + '.treebank.output'
            path2 = 'models-test-treebank-emb.old/' + couple2 + '--' + dataset + '/' + datasetName + '-um-covered-test.conllu.' + emb + '.treebank.output'
            if os.path.exists(path1):
                bestTest = path1
                break
            elif os.path.exists(path2):
                bestTest = path2
                break
        if bestTest == '':
            print("ERROR data+ext", dataset, couple2)
    # collect the test predictions
    if not os.path.exists(bestTest):
        print('ERROR', bestTest, 'not found!')
    else:
        cmd = 'cp ' + bestTest + ' ' + datasetDir + '/' + datasetName + '-um-test.conllu.output'
        #print(cmd)
        os.system(cmd)

cmd = 'chmod -R 755 ' + tgtDir
print(cmd)
cmd = 'tar -zcvf ' + tgtDir + '.tar.gz ' + tgtDir + '/'
print(cmd)

