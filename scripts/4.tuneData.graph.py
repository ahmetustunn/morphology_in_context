import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

fig=plt.figure(1,figsize=(16,20), dpi=300)
bigScores = []
smallScores = []

names = ['concat (-D)', '+D']
names2 = ['Small (concat)', 'Small (+D)', 'Large (concat)', 'Large (+D)']
for datasetIdx, dataset in enumerate(myutils.tuneSets):
    datasetName = myutils.getDatasetName(dataset)
    concatDir1 = 'models-test-wo-treebank/' + dataset + '--' + myutils.getCouple(dataset)
    concatDir2 = 'models-test-wo-treebank/' + myutils.getCouple(dataset) + '--' + dataset
    if os.path.isdir(concatDir1):
        concatScore = myutils.getScores(concatDir1 + '/' + datasetName + '.score')
    elif os.path.isdir(concatDir2):
        concatScore = myutils.getScores(concatDir2 + '/' + datasetName + '.score')
    else:
        print('ERR', dataset, ' not found')
        print(concatDir1)
        print(concatDir2)
        concatScore = [0.0,0.0,0.0,0.0]
    dataScore, couple = myutils.getTreeScores(dataset, 'models-test-treebank')
    scores = [[concatScore[0], concatScore[3]], [dataScore[0], dataScore[3]]]
    if 'PUD' not in dataset and 'CFL' not in dataset:
        bigScores.append(scores)
    else:
        smallScores.append(scores)
    ax = fig.add_subplot(5,2,1 + datasetIdx)
    fig.set_figheight(20)
    fig.set_figwidth(10)
    print(scores)
    myutils.drawGraph(ax,scores, ['lemma acc', 'morph f1'], names, '', '', (85,100), 'lower right')
    ax.set_title(dataset)
    if datasetIdx != 0 and datasetIdx != len(myutils.tuneSets)-1:
        ax.get_legend().remove()

finalData = []
for i in range(4):
    finalData.append([0.0, 0.0])
print(smallScores) 
for i in range(len(smallScores)):
    for j in range(len(smallScores[i])):
        print(smallScores[i][j])
        finalData[j][0] += smallScores[i][j][0]
        finalData[j][1] += smallScores[i][j][1]
for j in range(len(smallScores[0])):
    finalData[j][0] /= len(smallScores)
    finalData[j][1] /= len(smallScores)
for i in range(len(bigScores)):
    for j in range(len(bigScores[i])):
        finalData[j + len(smallScores[0])][0] += bigScores[i][j][0]
        finalData[j + len(smallScores[0])][1] += bigScores[i][j][1]
for j in range(len(bigScores[0])):
    finalData[j + len(smallScores[0])][0] /= len(bigScores)
    finalData[j + len(smallScores[0])][1] /= len(bigScores)
print()
print(finalData)

#ax = fig.add_subplot(5,2,1 + len(allScores))
#myutils.drawGraph(ax, avgScores, ['lemma acc', 'morph f1'], names, '','', (75,100))
#ax.set_title('Average')

plt.subplots_adjust(hspace=0.4)
plt.savefig('data.pdf', bbox_inches='tight')

fig, ax = plt.subplots(figsize=(8,3), dpi=300)
myutils.drawGraph(ax, finalData, ['lemma acc', 'morph f1'], names2, '','', (90,96), 'upper left')
plt.savefig('dataAvg.pdf', bbox_inches='tight')



