import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

fig, ax = plt.subplots(figsize=(8,3), dpi=300)

scores = [[85.38,91.53], [89.45, 86.75], [91.54, 87.65], [91.26, 89.70], [92.96, 91.29]]
names = ['base', '-E-D', '+E-D', '-E+D', '+E+D']

myutils.drawGraph(ax, scores, ['morph f1', 'lemma acc'], names, '','', (85,96), 'upper left')
#ax.patch.set_facecolor('none')
plt.savefig('devResults.pdf', bbox_inches='tight', edgecolor='white', facecolor='grey' )
#plt.savefig('devResults.pdf', bbox_inches='tight', edgecolor='white')



