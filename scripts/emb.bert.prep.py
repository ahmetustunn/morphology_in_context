import os

os.system('git clone https://github.com/google-research/bert.git')
os.system('git clone https://github.com/hanxiao/bert-as-service.git')

os.system('pip3 install --user tensorflow')
os.system('pip3 install --user bert-serving-server')
os.system('pip3 install --user bert-serving-client')

os.system('wget https://storage.googleapis.com/bert_models/2018_11_23/multi_cased_L-12_H-768_A-12.zip')
os.system('unzip multi_cased_L-12_H-768_A-12.zip')
if not os.path.exists('data/bert'):
    os.mkdir('data/bert')
os.system('mv multi_cased_L-12_H-768_A-12/* data/bert/')
os.system('rm -rf multi_cased_L-12_H-768_A-12*')


