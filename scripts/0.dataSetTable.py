import os
import subprocess

def wc(textPath):
    lines = subprocess.check_output("/usr/bin/grep -c \"^$\" " + textPath, shell=True)
    words = subprocess.check_output("/usr/bin/grep -c \"^[0-9]\" " + textPath, shell=True)
    return int(lines), int(words)
    #return len(open(textPath).readlines())

def getTrain(dataSet):
    for conlFile in os.listdir('data/' + dataset):
        if conlFile.endswith('train.conllu'):
            return 'data/' + dataset + '/' + conlFile
    return ''

fams = {'English':'IE,Germanic', 'Turkish':'Turkic,Southwestern', 'Chinese':'Sino-Tibetan', 'Finnish':'Uralic,Finnic'}
for dataset in myutils.tuneSets:
    lines, words = wc('data/' + dataset + '/*train.conllu')
    fam = ''
    for langFam in fams:
        if langFam in dataset:
            fam = fams[langFam]
    totalWords = 0
    totalTags = 0
    for line in open(getTrain(dataset)):
        tok = line.split('\t')
        if len(tok) == 10:
            totalWords += 1
            totalTags += len(tok[5].split(';'))
    print(dataset.replace('_', '\\_'), '&', fam, '&', "{:,}".format(lines), '&', "{:,}".format(words), '&', '{:,.2f}'.format(totalTags/totalWords), '\\\\')

