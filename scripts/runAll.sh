# If more than 6 arguments are given, it will run using slurm
function run {
    if [ "$7" ];
    then
        python3 scripts/pg.prep.py $1 $2 $3 $4 $5 $6
        python3 scripts/pg.run.py $2.[0-9]*
    else
        chmod +x $1
        bash $1
    fi
}

./scripts.emb.elmo.prep.sh
./scripts.emb.fast.prep.sh
./scripts.emb.poly.prep.sh

python3 scripts/3.tuneExt.prep.py > 3.prep.sh
chmod +x 3.prep.sh
./3.prep.sh
python3 scripts/3.tuneExt.run.py > 3.run.sh
run 3.run.sh 3.run 24 32 2 4 $1

python3 scripts/5.test.run.py > 5.run1.sh
run 5.run1.sh 5.run1 24 32 2 4 $1

python3 scripts/5.testEmb.prep.py > 5.prep.sh
chmod +x 5.prep.sh
./5.prep.sh

python3 scripts/5.testEmb.run.py > 5.run2.sh
run 5.run2.sh 5.run2 24 32 2 4 $1

#6?

