import os
import myutils

baseTestScores = {}
ownScores = {}
for metric in ['Lemma accuracy', 'Lemma Levenshtein', 'Morph accuracy', 'Morph F1']:
    for line in open('SharedTask2019 results - Task 2 (' + metric + ').csv'):
        tok = line.strip().split(',')
        treebank = tok[0]
        if treebank.startswith('UD'):
            if treebank not in baseTestScores:
                baseTestScores[treebank] = [] 
                ownScores[treebank] = []
            baseTestScores[treebank].append(float(tok[-1]))
            ownScores[treebank].append(float(tok[13]))

baseDevScores = {}
for line in open('Task 2 Baseline Dev Scores - Sheet1.csv'):
    if line[0] == ',':
        continue
    tok = line.split(',')
    baseDevScores[tok[0]] = [float(x) for x in (tok[1:])]



#[0] = lemma acc
#[1] = lemma lev
#[2] = morph acc
#[3] = morph f1
data = []
for j in range(8):
    data.append([])
    for i in range(4):
        data[-1].append(0.0)
devCounter = 0
testCounter = 0
for dataset in os.listdir('data'):
    datasetName = myutils.getDatasetName(dataset)
    devScores = myutils.getScores('models-test/' + dataset + '/score')
    devExtScores = myutils.getScores('models-test-emb/' + dataset + '/score')
    devDataScores, devDataSetting = myutils.getTreeScores(dataset, 'models-test-treebank/')
    devDataExtScores, devDataExtSetting = myutils.getTreeScores(dataset, 'models-test-treebank-emb.old/')
    if datasetName in baseDevScores:
        devBase = baseDevScores[datasetName]
    else:
        devBase = [0.0,0.0,0.0,0.0]


    devScore = (devScores[0] + devScores[3]) / 2
    devExtScore = (devExtScores[0] + devExtScores[3]) / 2
    devDataScore = (devDataScores[0] + devDataScores[3]) / 2
    devDataExtScore = (devDataExtScores[0] + devDataExtScores[3]) / 2
    highestNoExt = max(devScore, devDataScore)
    highest = max(devScore, devExtScore, devDataScore, devDataExtScore)

    testBase = baseTestScores[dataset]
    #[0] = dev -e
    #[1] = dev +e
    #[2] = test -e
    #[3] = test +
    
    testPath = 'data/' + dataset + '/' + datasetName + '-um-test.conllu'
    
    if highestNoExt == devScore:
        testPred = 'data/' + dataset + '/' + datasetName + '-um-covered-test.conllu.output'
        testScores = myutils.evaluate(testPred, testPath)
    else:
        testScores, couple = myutils.getTreeScoresTest(dataset, 'models-test-treebank')
    
    if highest == devExtScore:
        testExtPred = 'data/' + dataset + '/' + datasetName + '-um-covered-test.conllu.' + myutils.getEmbType(dataset) + '.output'
        if os.path.exists(testExtPred):
            testExtScores = myutils.evaluate(testExtPred, testPath)
        else:
            testExtScores = [0.0,0.0,0.0,0.0]
    elif highest == devDataExtScore:
        testExtScores, couple = myutils.getTreeScoresTest(dataset, 'models-test-treebank-emb.old')
    else:
        testExtScores = testScores

    if not (0.0 in devDataExtScores or 0.0 in devDataScores or 0.0 in devBase):
        devCounter += 1
        for modelIdx, scores in enumerate([devBase, devScores, devExtScores, devDataScores, devDataExtScores]):
            data[modelIdx][0] += scores[2]
            data[modelIdx][1] += scores[3]
            data[modelIdx][2] += scores[0]
            data[modelIdx][3] += scores[1]
    print(testScores)
    print(testExtScores)
    print()
    
    testCounter += 1
    for modelIdx, scores in enumerate([testBase, testScores, testExtScores]):
        data[modelIdx + 5][0] += scores[2]
        data[modelIdx + 5][1] += scores[3]
        data[modelIdx + 5][2] += scores[0]
        data[modelIdx + 5][3] += scores[1]

print()
for i in range(len(data)):
    print(data[i])
print(testCounter, devCounter)

for i in range(5):
    for j in range(len(data[0])):
        data[i][j] = data[i][j] / devCounter
for i in range(5,len(data)):
    print(i)
    for j in range(len(data[0])):
        data[i][j] = data[i][j] / testCounter

print()
for i in range(len(data)):
    print(data[i])

rows = ['base', '-E-D', '+E-D', '-E+D', '+E+D', 'base', '-E', '+E']
for i in range(len(data)):
    data[i] = [rows[i]] + data[i]

columns = ['Models', 'Acc', 'F1', 'Acc', 'Lev']
ordering = [False, False, False, False, True]
extra = '                 & \multicolumn{2}{c}{Morph. tags}      & \multicolumn{2}{c}{Lemma} \\\\'
extra += '\n        \\midrule'
cap = 'Average results for all evaluation metrics for development and test data. +E: use external embeddings for initialization. For the development data, only datasets for which predictions were available for all settings are used.'
myutils.makeTable(columns, data, 2, 'l | r r r r', 'tab:avg', cap, '', extra, ordering)

print("""please add dev/test distinction yourself:


        \midrule
        \multicolumn{4}{l}{test (all)}\\
        \midrule


        \midrule
        \multicolumn{4}{l}{dev (small)}\\
        \midrule
""")
