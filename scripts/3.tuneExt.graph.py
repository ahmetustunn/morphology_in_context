import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

def getScore(path, metric):
    if not os.path.exists(path):
        print('error, ' + path + ' does not exist')
        return 0.0
    for line in open(path):
        if line.startswith(metric):
            return float (line.strip().split()[-1])
    return 0.0


trans = {'char-hidden':'ch-hi', 'context-concat':'co-co', 'context-hidden':'co-hi', 'char':'ch', 'char-context':'ch-co'}
fig=plt.figure(1,figsize=(16,20), dpi=300)
allScores = []

for datasetIdx, dataset in enumerate(myutils.tuneSets):
    scores = []
    names = []
    for embs in ['', 'poly', 'fast', 'elmo']:
        scores.append([])
        model = 'models/embs.' + dataset + '.' + embs
        names.append(embs if embs != '' else 'none')
        scores[-1].append(getScore(os.path.join(model, 'score'), 'Lemma Acc'))
        scores[-1].append(getScore(os.path.join(model, 'score'), 'Morph F1'))
    print(dataset)
    print(names)
    print(scores)
    allScores.append(scores)
    print()
    ax = fig.add_subplot(5,2,1 + datasetIdx)
    fig.set_figheight(20)
    fig.set_figwidth(10)
    myutils.drawGraph(ax,scores, ['lemma acc', 'morph f1'], names, '', '', (75,100), 'lower right')
    ax.set_title(dataset)
    if datasetIdx != 0 and datasetIdx != len(myutils.tuneSets)-1:
        ax.get_legend().remove()

avgScores = [[]] * len(allScores[0])
for j in range(len(allScores[0])):
    avgScores[j] = [0.0,0.0]
    total = 0
    for i in [0,1,2,3,5]:
        if allScores[i][j][0] != 0.0:
            total += 1
            avgScores[j][0] += allScores[i][j][0]
            avgScores[j][1] += allScores[i][j][1]
    avgScores[j][0] = avgScores[j][0]/total
    avgScores[j][1] = avgScores[j][1]/total
print(avgScores)
#ax = fig.add_subplot(5,2,1 + len(allScores))
#myutils.drawGraph(ax, avgScores, ['lemma acc', 'morph f1'], names, '','', (75,100))
#ax.set_title('Average')

plt.subplots_adjust(hspace=0.4)
plt.savefig('ext.pdf', bbox_inches='tight')

fig, ax = plt.subplots(figsize=(8,3), dpi=300)
names[0] = 'none'
myutils.drawGraph(ax, avgScores, ['lemma acc', 'morph f1'], names, '','', (90,96), 'upper left')
plt.savefig('extAvg.pdf', bbox_inches='tight')



