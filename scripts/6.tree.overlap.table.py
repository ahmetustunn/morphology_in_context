import sys
import os
import myutils

data = {}
for dataset in os.listdir('data'):
    if not dataset.startswith('UD'):
        continue
    words = set()
    size = 0
    train, dev = myutils.getTrainDev('data/' + dataset)
    for line in open(train):
        tok = line.split('\t')
        if len(tok) < 2 or line[0] == '#':
            continue
        if tok[5] == 'NUM' or tok[5] == '_':
            continue
        words.add(tok[1]) 
        size += 1
    data[dataset] = words

def getOverlap(set1, set2):
    return len(set1.intersection(set2)) / len(set1)

def getEmbs(dataset):
    embs = set()
    for conlFile in os.listdir('data/' + dataset):
        for emb in ['elmo', 'fast', 'poly']:
            if conlFile.endswith(emb):
                embs.add(emb)
    return embs

table = []
for rowIdx, srcDataset in enumerate(sorted(data)):#[:50]:
    candidates = {}
    for tgtDataset in data:
        overlap = getOverlap(data[srcDataset], data[tgtDataset])
        if overlap != 0.0 and overlap != 1.0:
            candidates[tgtDataset] = overlap
    if len(candidates) == 0:
        add = [srcDataset, '', '']
    else:
        tgtDataset, overlap = sorted(candidates.items(), key=lambda kv: kv[1], reverse=True)[0]
    
        srcEmbs = getEmbs(srcDataset)
        tgtEmbs = getEmbs(tgtDataset)
        if 'elmo' in srcEmbs and 'elmo' in tgtEmbs:
            embType = 'elmo'
        elif 'poly' in srcEmbs and 'poly' in tgtEmbs:
            embType = 'poly'
        elif 'fast' in srcEmbs and 'fast' in tgtEmbs:
            embType = 'fast'
        else:
            embType = 'none'
        add = [srcDataset, tgtDataset, embType]
    add[0] = myutils.getDatasetName(add[0])
    add[1] = myutils.getDatasetName(add[1])
    table.append(add)

table = sorted(table)
newTable = []
for i in range(0,53):
    newTable.append(table[i] + table[i+54])

newTable.append(table[53] + ['','',''])
table = newTable

for i in range(len(table)):
    for j in range(len(table[i])):
        table[i][j] = table[i][j].replace('_', '\\_')

columns = ['Src Data','Additional', 'Emb. type']
columns = columns + columns
cap = ''
myutils.printTable(columns, table, 'l l l | l l l', 'tab:matching', cap, '', True)

#for srcDataset in sorted(data):
#    for tgtDataset in sorted(data):
#        overlap = getOverlap(data[srcDataset], data[tgtDataset])
        #if overlap > 0.2 and overlap < 0.4 and overlap != 1.0:
        #print(srcDataset, tgtDataset, "{0:.2f}".format(overlap * 100))
    

