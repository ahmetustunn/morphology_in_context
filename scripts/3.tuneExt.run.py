import os
import myutils

def getData(dataset, split, ending):
    if ending == '':
        ending = 'conllu'
    udDir = os.path.join('data', dataset)
    for file in os.listdir(udDir):
        if split in file and file.endswith(ending):
            return os.path.join(udDir, file)
    return ''


trans = {'ch-hi':'char-hidden', 'co-co':'context-concat', 'co-hi':'context-hidden', 'ch':'char', 'ch-co':'char-context'}

    #for strategy in [['ch-hi', 'ch', 'co-co', 'ch'], ['ch-hi', 'ch-co', 'co-co', 'ch'], ['co-hi', 'ch', 'ch-ci', 'ch'], ['co-hi', 'ch', 'ch-ci', 'ch-co']]:
for embeds, size in [('', 256),('poly',64), ('fast',300), ('elmo',1024), ('bert',768)]:
    for dataset in myutils.tuneSets:
        train = getData(dataset, 'train', embeds)
        dev = getData(dataset, 'dev', embeds)
        model = 'models/embs.' + dataset + '.' + embeds
        if os.path.exists(os.path.join(model, 'score')):
            continue
        cmd = 'python3 src/tagger.py  --train_size 50000 --model ' + model + ' --train ' + train + ' --dev ' + dev
        cmd += ' --enc_wordform_emb_size ' + str(size)
        if embeds != '':
            cmd += ' --enc_wordform_type pretrained'
        cmd += ' --morph_dec_model_type context-hidden'
        cmd += ' --morph_dec_attn_type char'
        cmd += ' --lemma_dec_model_type char-hidden'
        cmd += ' --lemma_dec_attn_type char'
        cmd += '  --train_size 250000'
        print(cmd)
    

