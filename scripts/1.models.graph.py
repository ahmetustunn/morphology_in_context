import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

def getScore(path, metric):
    if not os.path.exists(path):
        print('error, ' + path + ' does not exist')
        return 0.0
    for line in open(path):
        if line.startswith(metric):
            return float (line.strip().split()[-1])
    return 0.0


trans = {'char-hidden':'ch-hi', 'context-concat':'co-co', 'context-hidden':'co-hi', 'char':'ch', 'char-context':'ch-co'}
fig=plt.figure(1,figsize=(16,20), dpi=300)
allScores = []

for datasetIdx, dataset in enumerate(myutils.tuneSets):
    scores = []
    names = []
    for morphDec in ['char-hidden']:#, 'context-concat', 'context-hidden']:
        for lemmaDec in ['char-hidden']:#, 'context-concat', 'context-hidden']:
            for morphType in ['char', 'char-context']:
                for lemmaType in ['char', 'char-context']:
                    if lemmaType == 'char-context' and lemmaDec != 'char-hidden':
                        continue
                    if morphType == 'char-context' and morphDec != 'char-hidden':
                        continue
                    scores.append([])
                    model = 'models/' + '.'.join(['models', dataset, morphDec, lemmaDec, morphType, lemmaType])
                    names.append('.'.join([trans[morphDec], trans[morphType], trans[lemmaDec], trans[lemmaType]]))
                    scores[-1].append(getScore(os.path.join(model, 'score'), 'Lemma Acc'))
                    scores[-1].append(getScore(os.path.join(model, 'score'), 'Morph F1'))
    print(dataset)
    print(names)
    print(scores)
    allScores.append(scores)
    print()
    ax = fig.add_subplot(5,2,1 + datasetIdx)
    myutils.drawGraph(ax,scores, ['lemma acc', 'morph f1'], names, '', '', (75,100))
    ax.set_title(dataset)

avgScores = [[]] * len(allScores[0])
for j in range(len(allScores[0])):
    avgScores[j] = [0.0,0.0]
    total = 0
    for i in [0,1,2,3,5]:
        if allScores[i][j][0] != 0.0:
            total += 1
            avgScores[j][0] += allScores[i][j][0]
            avgScores[j][1] += allScores[i][j][1]
    avgScores[j][0] = avgScores[j][0]/total
    avgScores[j][1] = avgScores[j][1]/total
print(avgScores)
ax = fig.add_subplot(5,2,1 + len(allScores))
myutils.drawGraph(ax, avgScores, ['lemma acc', 'morph f1'], names, '','', (75,100))
ax.set_title('Average')

transString =''
for tran in sorted(trans):
    transString += trans[tran] + '=' + tran + '\n'
transString += '\nOrder: morphDec.morphType.lemmaDec.lemmaType'
plt.text(0, 35, transString, ha='left', wrap=True)
txt = 'Orange: Morph f1\nGreen:Lemma Acc'
plt.text(0, 30, txt, ha='left', wrap=True)
plt.subplots_adjust(hspace=0.7)
plt.savefig('models.pdf', bbox_inches='tight')

fig, ax = plt.subplots(figsize=(8,5), dpi=300)
myutils.drawGraph(ax, avgScores, ['lemma acc', 'morph f1'], names, '','', (86,96), 'lower right')
plt.savefig('modelsAvg.pdf', bbox_inches='tight')


