import os
import myutils

baseScores = {}
for line in open('Task 2 Baseline Dev Scores - Sheet1.csv'):
    if line[0] == ',':
        continue
    tok = line.split(',')
    baseScores[tok[0]] = [float(x) for x in (tok[1:])]

#[0] = lemma acc
#[1] = lemma lev
#[2] = morph acc
#[3] = morph f1
data = []
for dataset in sorted(os.listdir('data')):
    datasetName = myutils.getDatasetName(dataset)
    if datasetName in baseScores:
        base = baseScores[datasetName]
    else:
        base = [0.0, 0.0, 0.0, 0.0]
    ownScores = myutils.getScores('models-test/' + dataset + '/score')
    embScores = myutils.getScores('models-test-emb/' + dataset + '/score')
    treeScores, setting = myutils.getTreeScores(dataset,'models-test-treebank/')
    treeEmbScores, setting = myutils.getTreeScores(dataset, 'models-test-treebank-emb.old/')
    arScoreBase = (base[0] + base[3])/2
    arScore = (ownScores[0] + ownScores[3])/2
    arScoreE = (embScores[0] + embScores[3])/2
    arScoreD = (treeScores[0] + treeScores[3])/2
    arScoreDE = (treeEmbScores[0] + treeEmbScores[3])/2
    row = [datasetName, arScoreBase, arScore, arScoreD, arScoreE, arScoreDE]
    data.append(row)
    highest = max(row[2:])
    if highest == arScore:
        row += [ownScores[0], ownScores[3]]
    elif highest == arScoreD:
        row += [treeScores[0], treeScores[3]]
    elif highest == arScoreE:
        row += [embScores[0], embScores[3]]
    elif highest == arScoreDE:
        row += [treeEmbScores[0], treeEmbScores[3]]
    #potential additions lemma lev, morph acc, matching treebank

data = sorted(data)

totals = [0.0] * len(data[0])
totalNums = [0] * len(data[0])
for i in range(len(data)):
    for j in range(1,len(data[0])):
        if data[i][j] != 0.0:
            totalNums[j] += 1
        totals[j] += data[i][j]

data.append(['Macro Avg.'])
for i in range(1, len(totals)):
    if totalNums[i] == 0:
        data[-1].append(0.0)
    else:
        data[-1].append(totals[i] / totalNums[i])

for dataIdx in range(len(data)):
    highest = max(data[dataIdx][2:6])
    for i in range(2,6):
        if data[dataIdx][i] == highest:
            data[dataIdx][i] = '\\textbf{' + '{:.2f}'.format(data[dataIdx][i]) + '}'
        else:
            data[dataIdx][i] = '{:.2f}'.format(data[dataIdx][i])
    data[dataIdx][0] = data[dataIdx][0].replace('_', '\\_')
    for i in range(6,len(row)):
        data[dataIdx][i] = '{:.2f}'.format(data[dataIdx][i])
    data[dataIdx][1] = '{:.2f}'.format(data[dataIdx][1])

table = []
for i in range(54):
    table.append(data[i] + data[i + 54])

headers = ['Dataset', 'Base', '-E-D', '-E+D', '+E-D', '+E+D', 'Lem', 'Mor'] 
headers = headers + headers
allignment = 'l | r | r r r r | r r || l | r | r r r r | r r'
cap = 'Results on all development datasets. The average of lemma accuracy and morphological F1 score is used as main metric. base: baseline. E: external embeddings. D: dataset embeddings. Bold indicates which model is used on the test data. Lem: lemma accuracy of the bold model. Mor: morphologic tagging F1 score of bold model. For the macro average, the surprise datasets are ignored for the baseline model as there was no baseline score provided.'
myutils.printTable(headers, table, allignment, 'tab:devSelection', cap, '', True) 

