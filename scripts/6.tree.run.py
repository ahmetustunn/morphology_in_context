import os
import sys

def getDatasets(dataset):
    train = dev = test = ""
    for conlFile in os.listdir('data-treebank/' + dataset):
        if conlFile.endswith('train.conllu'):
            train = conlFile
        if conlFile.endswith('dev.conllu'):
            dev = conlFile
        if conlFile.endswith('test.conllu'):
            test = conlFile
    return os.path.join('data-treebank', dataset, train), os.path.join('data-treebank', dataset, dev), os.path.join('data-treebank', dataset,test)


for dataset in os.listdir('data-treebank'):
    train, dev, test = getDatasets(dataset)
    #if os.path.isfile('models-test/' + dataset + '/score'):
    #    continue
    cmd = 'python3 src/tagger.py --model models-test-treebank/' + dataset
    cmd += ' --train ' + train
    cmd += ' --dev ' + dev
    cmd += ' --test ' + test
    cmd += ' --train_size 500000 --treebank_emb_size 32'
    print(cmd)



