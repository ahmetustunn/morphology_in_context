import os
import myutils

models = [['char-hidden', 'char-context', 'char-hidden', 'char']]#,
#         ['char-hidden', 'char-context', 'context-hidden', 'char'],
#         ['context-concat', 'char', 'context-concat', 'char']]

for char_emb in ['64', '128']:
    for word_emb in ['128', '256']:
        for hidden in ['512', '1024']:
            for dec_emb in ['128', '256']:
                for m in models:
                    for dataset in myutils.tuneSets:
                        model = 'models/embSize.' + '.'.join([dataset, m[0],m[1],m[2],m[3],char_emb, word_emb, hidden, dec_emb])
                        if os.path.exists(os.path.join(model, 'score')):
                            continue
                        train, dev = myutils.getTrainDev('data/' + dataset)
                        cmd = 'python3 src/tagger.py  --train_size 50000 --model ' + model + ' --train ' + train + ' --dev ' + dev + ' --test ' + dev
                        cmd += ' --morph_dec_model_type ' + m[0] + ' --morph_dec_attn_type ' + m[1]
                        cmd += ' --lemma_dec_model_type ' + m[2] + ' --lemma_dec_attn_type ' + m[3]
                        cmd += ' --enc_char_emb_size ' + char_emb
                        cmd += ' --enc_wordform_emb_size ' + word_emb
                        cmd += ' --enc_char_hidden_size ' + hidden
                        cmd += ' --enc_word_hidden_size ' + hidden
                        cmd += ' --dec_hidden_size ' + hidden
                        cmd += ' --dec_emb_size ' + dec_emb
                        cmd += ' --train_size 250000'
                        print(cmd)

