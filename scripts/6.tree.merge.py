import os
import sys


def merge_treebanks(files, max_word):
    o = open(files[-1], 'w', encoding='utf-8')

    for tb in files[:-1]:
        is_train = 'train' in os.path.split(tb)[1]
        if not is_train: tr = open(os.path.join(os.path.split(files[-1])[0], os.path.split(tb)[1]+'.treebank'), 'w', encoding='utf-8')
        with open(tb, 'r', encoding='utf-8') as t:
            word_count = 0
            max = False
            for line in t:
                if line != '\n' and not line.startswith('#'):
                    form = line.rstrip('\n').split('\t')
                    form[-1] += '|treebank='+os.path.basename(tb).split('-')[0]
                    if not is_train: tr.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(*form))
                    o.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(*form))

                    word_count += 1
                    if word_count >= max_word:
                       max = True
                else:
                    if not is_train: tr.write(line)
                    o.write(line)
                    if max:
                        break
        if not is_train: tr.close()
    o.close()


def merge_dir(dirs, max_word):
    train = []
    dev = []
    test = []

    target = ''
    for dir in dirs[:-2]:
        target =  target + '--' + os.path.split(dir)[1]
    target = os.path.join(dirs[-2], target[2:])
    if not os.path.isdir(target): os.mkdir(target)

    pretrained = ['elmo', 'fast', 'poly', 'none']
    assert dirs[-1] in pretrained

    if dirs[-1] == 'none':
        dirs[-1] = ''
        t = ''
    else:
        t = '.' +dirs[-1]

    for dir in dirs[:-2]:
        for file in os.listdir(dir):
            if 'train' in file and file.endswith(dirs[-1]) and 'output' not in file:
                train.append(os.path.join(dir, file))
            elif 'dev' in file and file.endswith(dirs[-1]) and 'output' not in file:
                dev.append(os.path.join(dir, file))
            elif 'test' in file and file.endswith(dirs[-1]) and 'output' not in file:
                test.append(os.path.join(dir, file))

    assert len(train) == len(dev) == len(test)
    train.append(os.path.join(target, 'train.conllu' + t))
    test.append(os.path.join(target, 'test.conllu' + t))
    dev.append(os.path.join(target, 'dev.conllu' + t))

    merge_treebanks(train, max_word)
    merge_treebanks(dev, max_word)
    merge_treebanks(test, max_word)


def get_overlap(overlap_txt):
    dataset_pairs =[]
    pair = []
    for line in open(overlap_txt):
        line = line.rstrip('\n').split()
        if len(line) >= 1:
            pair.extend(line)
        else:
            dataset_pairs.append(pair)
            pair = []
    return dataset_pairs


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print('please specify multiple treebank to be merged')
    else:
        for pair in get_overlap('scripts/6.tree.overlap.txt'):
            pair.pop(0)
            max_word = 250000
            if sys.argv[1] == 'pretrained':
                merge_dir([os.path.join('data', pair[0]),
                            os.path.join('data', pair[3]),
                            'data-treebank',
                            pair[2]], max_word)
            else:
                merge_dir([os.path.join('data', pair[0]),
                            os.path.join('data', pair[3]),
                            'data-treebank',
                            'none'], max_word)
