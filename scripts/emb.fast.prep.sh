
mkdir data/fasttext
curl https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.en.300.bin.gz | gunzip > embs/fasttext/en
curl https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.tr.300.bin.gz | gunzip > embs/fasttext/tr
curl https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.fi.300.bin.gz | gunzip > embs/fasttext/fi
curl https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.zh.300.bin.gz | gunzip > embs/fasttext/zh
wget https://dl.fbaipublicfiles.com/fasttext/vectors-wiki/wiki.bm.zip && unzip wiki.bm && mv wiki.bm.bin embs/fasttext/bm && rm wiki.bm.*
wget https://dl.fbaipublicfiles.com/fasttext/vectors-wiki/wiki.bxr.zip && unzip wiki.bxr && mv wiki.bxr.bin embs/fasttext/bxr && rm wiki.bxr.*
wget https://dl.fbaipublicfiles.com/fasttext/vectors-wiki/wiki.got.zip && unzip wiki.got && mv wiki.got.bin embs/fasttext/got && rm wiki.got.*
wget https://dl.fbaipublicfiles.com/fasttext/vectors-wiki/wiki.kv.zip && unzip wiki.kv && mv wiki.kv.bin embs/fasttext/kv && rm wiki.kv.*


git clone https://github.com/facebookresearch/fastText.git
cd fastText
sed -i "s;-march=native;;g" Makefile
make 
cd ..

