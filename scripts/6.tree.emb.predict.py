import os
import sys


def get_scripts(dir):

    dev = []
    test = []
    output= []

    re_run = True
    if os.path.isdir(dir):
        for file in os.listdir(dir):
            if file.endswith('.score'):
                re_run = False
                continue

    data_dir = os.path.join('data-treebank',os.path.split(dir)[1].split('--')[1]+ '--'+ os.path.split(dir)[1].split('--')[0])
    if re_run:
        for file in os.listdir(data_dir):
            if not file.endswith('test.conllu.treebank') and 'test.conllu' in file and 'treebank' in file:
                test.append(os.path.join(data_dir, file))
            elif not file.endswith('dev.conllu.treebank') and 'dev.conllu' in file and 'treebank' in file:
                dev.append(os.path.join(data_dir, file))
                output.append(os.path.join(dir, file + '.output'))

    if len(dev) == len(test) != 0:

        echo_predict = 'python3 src/tagger.py --model ' + dir
        echo_eval = []

        for i in range(len(dev)):
            echo_predict += ' --predict ' + dev[i]
            echo_predict += ' --predict ' + test[i]
            echo_eval.append('python3 src/eval.py ' + dev[i] + ' ' + output[i] + ' > ' + os.path.join(dir, os.path.basename(dev[i]).split('-')[0] + '.score'))

        print(echo_predict)
        for i in echo_eval:
            print(i)


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print('please specify merged treebank dir and model')
    else:
        for dir in os.listdir('models-test-treebank-emb.old'):
            get_scripts(os.path.join('models-test-treebank-emb.old', dir))
