import os

def getDatasets(dataset):
    train = dev = test = ""
    for conlFile in os.listdir('data/' + dataset):
        if 'train' in conlFile and conlFile.endswith('.conllu'):
            train = conlFile
        if 'dev' in conlFile and conlFile.endswith('.conllu'):
            dev = conlFile
        if 'test' in conlFile and conlFile.endswith('.conllu'):
            test = conlFile
    return os.path.join('data', dataset, train), os.path.join('data', dataset, dev), os.path.join('data', dataset,test)

for dataset in os.listdir('data'):
    train, dev, test = getDatasets(dataset)
    #if os.path.isfile('models-test/' + dataset + '/score'):
    #    continue
    cmd = 'python3 src/tagger.py --model models-test/' + dataset 
    cmd += ' --train ' + train
    cmd += ' --dev ' + dev
    cmd += ' --test ' + test
    cmd += ' --train_size 250000' 
    print(cmd)


