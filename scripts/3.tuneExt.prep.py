import os
import myutils

port = 5555 

for dataset in myutils.tuneSets:
    embLang = myutils.getLang(dataset)
    for conlFile in myutils.getTrainDev('data/' + dataset):
        # Bert
        if not os.path.exists(conlFile + '.bert'):
            cmd = 'python3 scripts/emb.bert.py embs/bert ' + conlFile + ' ' + str(port)
            port += 2
            print(cmd)

        # Elmo
        if not os.path.exists(conlFile + '.elmo'):
            cmd = 'cd ELMoForManyLangs'
            cmd += ' && python3 emb.elmo.py ' + embLang + ' ../' + conlFile + ' > ../' + conlFile + '.elmo'
            cmd += ' && cd ../'
            print(cmd)

        # Fast
        if not os.path.exists(conlFile + '.fast'):
            cmd = 'python3 scripts/emb.fast.py embs/fasttext/' + embLang + ' ' + conlFile
            print(cmd)

        # Poly
        if not os.path.exists(conlFile + '.poly'):
            cmd = 'python3 scripts/emb.poly.py embs/polyglot/' + embLang + '.pickle ' + conlFile
            print(cmd)

        

