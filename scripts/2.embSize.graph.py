import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

def getScore(path, metric):
    if not os.path.exists(path):
        print('error, ' + path + ' does not exist')
        return 0.0
    for line in open(path):
        if line.startswith(metric):
            return float (line.strip().split()[-1])
    return 0.0

models = [['char-hidden', 'char-context', 'char-hidden', 'char']]#,
         #['char-hidden', 'char-context', 'context-hidden', 'char'],
         #['context-concat', 'char', 'context-concat', 'char']]

trans = {'char-hidden':'ch-hi', 'context-concat':'co-co', 'context-hidden':'co-hi', 'char':'ch', 'char-context':'ch-co'}
fig=plt.figure(1,figsize=(16,20), dpi=300)

allScores = []

for datasetIdx, dataset in enumerate(myutils.tuneSets):
    for mIdx, m in enumerate(models):
        scores = []
        names = []
        for char_emb in ['64', '128']:
            for word_emb in ['128', '256']:
                for hidden in ['512', '1024']:
                    for dec_emb in ['128', '256']:
                        scores.append([])
                        model = 'models/' + '.'.join(['embSize', dataset, m[0],m[1],m[2],m[3],char_emb, word_emb, hidden, dec_emb])
                        names.append('.'.join([char_emb, word_emb, hidden, dec_emb]))
                        scores[-1].append(getScore(os.path.join(model, 'score'), 'Lemma Acc'))
                        scores[-1].append(getScore(os.path.join(model, 'score'), 'Morph F1'))
        print(dataset)
        print(names)
        print(scores)
        allScores.append(scores)
        print()
        ax = fig.add_subplot(5,2,1 + datasetIdx)
        myutils.drawGraph(ax,scores, ['lemma acc', 'morph f1'], names, '', '', (75,100))
        ax.set_title('.'.join([trans[m[0]], trans[m[1]], trans[m[2]], trans[m[3]], dataset]) )
        ax.get_legend().remove()

avgScores = [[]] * len(allScores[0])
for j in range(len(allScores[0])):
    avgScores[j] = [0.0,0.0]
    total = 0
    for i in range(len(allScores)):
        if allScores[i][j][0] != 0.0:
            total += 1
            avgScores[j][0] += allScores[i][j][0]
            avgScores[j][1] += allScores[i][j][1]
    avgScores[j][0] = avgScores[j][0]/total if total != 0 else 0
    avgScores[j][1] = avgScores[j][1]/total if total != 0 else 0

print(avgScores)
ax = fig.add_subplot(5,2,1 + len(allScores) )
myutils.drawGraph(ax, avgScores, ['lemma acc', 'morph f1'], names, '','', (75,100))
ax.set_title('Average')

transString =''
for tran in sorted(trans):
    transString += trans[tran] + '=' + tran + '\n'
plt.text(10, 300, transString, ha='left', wrap=True)
plt.subplots_adjust(hspace=0.9)
transString = 'Order: charEmb, wordEmb, hidden, decEmb'
plt.text(-10, 300, transString, ha='left', wrap=True)

plt.savefig('embSize.pdf', bbox_inches='tight')

fig, ax = plt.subplots(figsize=(8,5), dpi=300)
myutils.drawGraph(ax, avgScores, ['lemma acc', 'morph f1'], names, '','', (86,96))
plt.savefig('embSizeAvg.pdf', bbox_inches='tight')



