import os
import sys


def getDatasets(dataset):
    train = dev = test = emb = ""
    for conlFile in os.listdir('data-treebank/' + dataset):
        if not conlFile.endswith('train.conllu') and 'train.conllu' in conlFile and 'treebank' not in conlFile:
            train = conlFile
        if not conlFile.endswith('dev.conllu') and 'dev.conllu' in conlFile and 'treebank' not in conlFile:
            dev = conlFile
        if not conlFile.endswith('test.conllu') and 'test.conllu' in conlFile and 'treebank' not in conlFile:
            test = conlFile
        emb = train.split('.')[-1]
    return os.path.join('data-treebank', dataset, train), os.path.join('data-treebank', dataset, dev), os.path.join('data-treebank', dataset,test), emb


for dataset in os.listdir('data-treebank'):
    sizes = {'': 256, 'poly': 64, 'fast': 300, 'elmo': 1024, 'bert': 768}
    train, dev, test, emb = getDatasets(dataset)
    #if os.path.isfile('models-test/' + dataset + '/score'):
    #    continue
    cmd = 'python3 src/tagger.py --model models-test-treebank-emb/' + dataset
    cmd += ' --train ' + train
    cmd += ' --dev ' + dev
    cmd += ' --test ' + test
    cmd += ' --enc_wordform_emb_size ' + str(sizes[emb])
    cmd += ' --train_size 500000 --enc_wordform_type pretrained --treebank_emb_size 32 '
    print(cmd)



