import sys
import pickle

if len(sys.argv) < 3:
    print('please provide embeddings and conl file')
    exit(0)

def loadEmbs(path):
    print('Loading ' + path + '...')
    words, vect = pickle.load(open(path, 'rb'), encoding='latin1')
    embs = {}
    for i in range(len(words)):
        embs[words[i]] = vect[i]
    return embs
embs = loadEmbs(sys.argv[1])

unk = '<UNK>'

outFile = open(sys.argv[2] + '.poly', 'w')
curTree = ''
for line in open(sys.argv[2]):
    if len(line) < 2:
        outFile.write(curTree + '\n')
        curTree = '' 
    elif line[0] == '#':
        curTree += line
    else:
        tok = line.strip().split('\t')
        if tok[1] in embs:
            emb = embs[tok[1]]
        elif tok[1].lower() in embs:
            emb = embs[tok[1].lower()]
        else:
            emb = embs[unk]

        embStr = 'emb=' + ','.join([str(x) for x in emb])
        if tok[-1] == '_':
            tok[-1] = embStr
        else:
            tok[-1] += '|' + embStr
        curTree += '\t'.join(tok) + '\n'

outFile.close()
