import sys
import os
from bert_serving.server.helper import get_args_parser
from bert_serving.server import BertServer
from bert_serving.client import BertClient
import time


if len(sys.argv) < 4:
    print('please provide embeddings, conl file and port')
    exit(0)

port1 = int(sys.argv[3])
port2 = port1 + 1

args = get_args_parser().parse_args(['-model_dir', sys.argv[1],
                                     '-port', str(port1),
                                     '-port_out', str(port2),
                                     '-max_seq_len', 'NONE',
                                     '-pooling_strategy', 'NONE',
                                     '-mask_cls_sep',
                                     '-cpu'])
print('starting bert')
server = BertServer(args)
server.start()
print('started')

#os.system('bert-serving-start -pooling_strategy NONE -model_dir ' + sys.argv[1] + '  -num_worker=1 > /dev/null 2> /dev/null &')

time.sleep(30)#is this necessary?

print('starting client')
bc = BertClient(port=port1, port_out=port2)
print('done')
time.sleep(30)# is this necessary?

curComments = ''
curTree = []
outFile = open(sys.argv[2] + '.bert', 'w')
for line in open(sys.argv[2]):
    if len(line) < 2:
        outFile.write(curComments.strip() + '\n')
        sent = [x[1] for x in curTree]
        embs = bc.encode([sent], is_tokenized=True)[0]
        for wordIdx in range(len(curTree)):
            embStr = 'emb=' + ','.join([str(x) for x in embs[wordIdx+1]])
            if curTree[wordIdx][-1] == '_':
                curTree[wordIdx][-1] = embStr
            else:
                curTree[wordIdx][-1] += '|' + embStr
            outFile.write('\t'.join(curTree[wordIdx]) + '\n')
        outFile.write('\n')
        curComments = ''
        curTree = []
    elif line[0] == '#':
        curComments += line
    else:
        tok = line.strip().split('\t')
        curTree.append(tok)
outFile.close()
os.system('bert-serving-terminate -port ' + str(port1))

