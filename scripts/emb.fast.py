import sys
import os
if len(sys.argv) < 3:
    print('please provide embeddings and conl file')
    exit(0)


#TODO could be more efficient by only havine uniq words?
conlFile = sys.argv[2]
embFile = conlFile.replace('conllu', 'embeds')
wordFile = conlFile.replace('conllu', 'words')
cmd = 'grep -v "^#" ' + conlFile + ' | cut -f 2 | grep -v "^$" > ' + wordFile
print(cmd)
os.system(cmd)
cmd = "cd fastText && ./fasttext print-word-vectors ../" + sys.argv[1] + ' < ../' + wordFile + ' > ../' + embFile + ' && cd ../'
print(cmd)
os.system(cmd)

embs = []
for line in open(embFile):
    line = line.strip()[line.find(' ')+1:].replace(' ',',')
    embs.append(line)

outFile = open(conlFile + '.fast', 'w')
wordIdx = 0
for line in open(conlFile):
    if len(line) <= 2:
        outFile.write(line)
    elif line[0] == '#':
        outFile.write(line)
    else:
        tok = line.strip().split('\t')
        #TODO
        embStr = 'emb=' + embs[wordIdx]
        if tok[-1] == '_':
            tok[-1] = embStr
        else:
            tok[-1] += '|' + embStr
        outFile.write('\t'.join(tok) + '\n')
        wordIdx += 1
outFile.close()

cmd = 'rm ' + embFile + ' ' + wordFile
print(cmd)
os.system(cmd)
