import os
import myutils

#[0] = lemma acc
#[1] = lemma lev
#[2] = morph acc
#[3] = morph f1
data = []
counter = 0
totals = [0.0, 0.0, 0.0, 0.0]
for dataset in sorted(os.listdir('data')):
    datasetName = myutils.getDatasetName(dataset)
    counter += 1

    #tree = dataset embs
    #emb = external embs
    ownScores = myutils.getScores('models-test/' + dataset + '/score')
    embScores = myutils.getScores('models-test-emb/' + dataset + '/score')
    treeScores, setting = myutils.getTreeScores(dataset,'models-test-treebank/')
    treeEmbScores, setting = myutils.getTreeScores(dataset, 'models-test-treebank-emb.old/')
    arScore = (ownScores[0] + ownScores[3])/2
    arScoreE = (embScores[0] + embScores[3])/2
    arScoreD = (treeScores[0] + treeScores[3])/2
    arScoreED = (treeEmbScores[0] + treeEmbScores[3])/2
    highest = max([arScoreED, arScoreD, arScoreE, arScore])
    testPath = 'data/' + dataset + '/' + datasetName + '-um-test.conllu'
    if highest == arScoreED:
        setting = [True, True]
        testResults, couple = myutils.getTreeScoresTest(dataset, 'models-test-treebank-emb.old')
    elif highest == arScoreD:
        setting = [False, True]
        testResults, couple = myutils.getTreeScoresTest(dataset, 'models-test-treebank')
    elif highest == arScoreE:
        setting = [True, False]
        testPred = 'data/' + dataset + '/' + datasetName + '-um-covered-test.conllu.' + myutils.getEmbType(dataset) + '.output'
        testResults = myutils.evaluate(testPath, testPred)
    elif highest == arScore:
        setting = [False, False]
        testPred = 'data/' + dataset + '/' + datasetName + '-um-covered-test.conllu.output'
        testResults = myutils.evaluate(testPath, testPred)

    print(testResults, setting)
    data.append([datasetName] + testResults + setting)
    for i in range(len(totals)):
        totals[i] += testResults[i]
    
data = sorted(data)
for i in range(len(totals)):
    totals[i] /= counter
data.append(["average"] + totals + ['', ''])

for i in range(len(data)):
    data[i][0] = data[i][0].replace('_', '\\_')
    for j in range(1,len(data[i])):
        if type(data[i][j]) == bool:
            data[i][j] = '+' if data[i][j] else '-'
        elif type(data[i][j]) == float:
            data[i][j] = '{:.2f}'.format(data[i][j])

table = []
for i in range(54):
    table.append(data[i] + data[i + 54])

extra = '                        '
extra += '& \multicolumn{2}{c}{Lemma} & \multicolumn{2}{c}{Morph. tags} & \multicolumn{2}{c||}{Setting}'
extra += ' & \n'
extra += '& \multicolumn{2}{c}{Lemma} & \multicolumn{2}{c}{Morph. tags} & \multicolumn{2}{c}{Setting}'
extra += '\\\\\n'
extra += '        \\midrule\n'
headers = ['Dataset', 'Acc', 'Lev', 'Acc', 'F1', 'E', 'D']
headers = headers + headers
allignment = 'l | r r r r | c c || l | r r r r | c c'
cap = 'All four evaluation metrics for the test data of our best system. E: use of external embeddings. D: use of dataset embeddings. Results might be different compared to the ones in the overview paper, as we did not have enough time to run all experiments before the deadline. The dataset embeddings were previously only used for the 40 smallest datasets'
myutils.printTable(headers, table, allignment, 'tab:testAll', cap, extra, True) 

