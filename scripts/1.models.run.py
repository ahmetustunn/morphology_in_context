import os
import myutils

for dataset in myutils.tuneSets:
    for morphDec in ['char-hidden']:#, 'context-concat', 'context-hidden']:
        for lemmaDec in ['char-hidden']:#, 'context-concat', 'context-hidden']:
            for morphType in ['char', 'char-context']:
                for lemmaType in ['char', 'char-context']:
                    if lemmaType == 'char-context' and lemmaDec != 'char-hidden':
                        continue
                    if morphType == 'char-context' and morphDec != 'char-hidden':
                        continue
                    model = 'models/' + '.'.join(['models', dataset, morphDec, lemmaDec, morphType, lemmaType])
                    if os.path.exists(os.path.join(model, 'score')):
                        continue
                    train, dev  = myutils.getTrainDev('data/' + dataset)
                    cmd = 'python3 src/tagger.py  --train_size 50000 --model ' + model + ' --train ' + train + ' --dev ' + dev
                    cmd += ' --morph_dec_model_type ' + morphDec
                    cmd += ' --morph_dec_attn_type ' + morphType
                    cmd += ' --lemma_dec_model_type ' + lemmaDec
                    cmd += ' --lemma_dec_attn_type ' + lemmaType
                    cmd += ' --train_size 250000'
                    print(cmd)
    

