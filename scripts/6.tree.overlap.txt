False UD_Chinese-CFL 4871 elmo
UD_Chinese-GSD 78775

True UD_Korean-Kaist 244647 elmo
UD_Korean-PUD 11618

True UD_Spanish-GSD 297701 poly
UD_Yoruba-YTB 1789

True UD_Czech-PDT 1006136 poly
UD_Upper_Sorbian-UFAL 6871

False UD_Breton-KEB 7004 poly
UD_Norwegian-Bokmaal 218607

False UD_English-EWT 175695 elmo
UD_Naija-NSC 7613

False UD_Slovenian-SSJ 96080 elmo
UD_Slovenian-SST 21993

False UD_Arabic-PADT 189138 elmo
UD_Arabic-PUD 14832

False UD_Persian-Seraji 108947 elmo
UD_Urdu-UDTB 103254

False UD_Latvian-LVTB 99081 poly
UD_Lithuanian-HSE 3402

False UD_Faroese-OFT 6698 poly
UD_Norwegian-Nynorsk 213154

True UD_Buryat-BDT 6404 fast
UD_Russian-SynTagRus 714236

True UD_Ancient_Greek-PROIEL 170834 elmo
UD_Ancient_Greek-Perseus 142036

False UD_English-EWT 175695 elmo
UD_English-PUD 14584

False UD_Croatian-SET 136349 poly
UD_Serbian-SET 59159

False UD_English-EWT 175695 elmo
UD_English-ParTUT 33856

True UD_Czech-FicTree 107703 elmo
UD_Czech-PDT 1006136

False UD_English-EWT 175695 elmo
UD_English-LinES 57887

True UD_Danish-DDT 68165 elmo
UD_Norwegian-Bokmaal 218607

False UD_Italian-ISDT 207813 elmo
UD_Italian-PUD 16592

False UD_Polish-LFG 84120 elmo
UD_Polish-SZ 55491

False UD_English-EWT 175695 elmo
UD_Vietnamese-VTB 26692

True UD_French-GSD 275646 elmo
UD_French-Spoken 27687

True UD_Italian-ISDT 207813 elmo
UD_Italian-PoSTWITA 75895

False UD_North_Sami-Giella 18083 poly
UD_Norwegian-Nynorsk 213154

False UD_Hebrew-HTB 113060 elmo
UD_Russian-GSD 64649

True UD_Czech-PDT 1006136 elmo
UD_Irish-IDT 16571

True UD_Old_Church_Slavonic-PROIEL 45883 elmo
UD_Russian-SynTagRus 714236

True UD_Belarusian-HSE 5383 poly
UD_Russian-SynTagRus 714236

True UD_Russian-SynTagRus 714236 elmo
UD_Ukrainian-IU 75361

True UD_Bulgarian-BTB 107067 elmo
UD_Russian-SynTagRus 714236

True UD_Galician-CTG 98216 elmo
UD_Spanish-AnCora 382495

False UD_Norwegian-Nynorsk 213154 elmo
UD_Norwegian-NynorskLIA 8817

False UD_Japanese-GSD 128200 elmo
UD_Japanese-PUD 18626

False UD_Afrikaans-AfriBooms 34358 poly
UD_Dutch-Alpino 145189

True UD_French-GSD 275646 elmo
UD_French-Sequoia 48693

True UD_Korean-GSD 55176 elmo
UD_Korean-Kaist 244647

False UD_Swedish-LinES 56142 elmo
UD_Swedish-Talbanken 68885

True UD_French-GSD 275646 elmo
UD_German-GSD 197389

False UD_Finnish-PUD 10498 elmo
UD_Finnish-TDT 134560

True UD_Spanish-GSD 297701 poly
UD_Tagalog-TRG 184

True UD_Russian-SynTagRus 714236 elmo
UD_Russian-Taiga 12770

False UD_Armenian-ArmTDP 14885 poly
UD_Russian-PUD 13021

True UD_Basque-BDT 77605 elmo
UD_Spanish-AnCora 382495

False UD_Finnish-FTB 108607 elmo
UD_Finnish-TDT 134560

False UD_Croatian-SET 136349 elmo
UD_Latvian-LVTB 99081

True UD_Kurmanji-MG 6872 poly
UD_Spanish-GSD 297701

True UD_Komi_Zyrian-IKDP 639 fast
UD_Russian-SynTagRus 714236

False UD_Ancient_Greek-PROIEL 170834 elmo
UD_Greek-GDT 44391

True UD_Czech-PDT 1006136 elmo
UD_Slovak-SNK 68479

False UD_Latin-PROIEL 159363 elmo
UD_Latin-Perseus 19641

True UD_Akkadian-PISANDUB 1397 elmo
UD_Czech-PDT 1006136

False UD_Japanese-GSD 128200 elmo
UD_Japanese-Modern 11185

True UD_Latin-ITTB 241497 elmo
UD_Latin-PROIEL 159363

False UD_Cantonese-HK 3912 poly
UD_Chinese-GSD 78775

True UD_Russian-PUD 13021 elmo
UD_Russian-SynTagRus 714236

True UD_Gothic-PROIEL 44569 none
UD_Norwegian-Nynorsk 213154

True UD_Portuguese-Bosque 153592 elmo
UD_Portuguese-GSD 213876

True UD_Estonian-EDT 287552 elmo
UD_Hungarian-Szeged 28782

False UD_Dutch-Alpino 145189 elmo
UD_Dutch-LassySmall 63257

True UD_Komi_Zyrian-Lattice 1356 fast
UD_Russian-SynTagRus 714236

True UD_Hindi-HDTB 258200 poly
UD_Sanskrit-UFAL 1285

True UD_Bambara-CRB 9547 fast
UD_Czech-PDT 1006136

True UD_Russian-GSD 64649 elmo
UD_Russian-SynTagRus 714236

True UD_Spanish-AnCora 382495 elmo
UD_Spanish-GSD 297701

False UD_Galician-CTG 98216 elmo
UD_Galician-TreeGal 18405

True UD_Hindi-HDTB 258200 poly
UD_Marathi-UFAL 2428

False UD_Chinese-GSD 78775 elmo
UD_Japanese-GSD 128200

True UD_Czech-PDT 1006136 elmo
UD_Czech-PUD 12483

False UD_Croatian-SET 136349 elmo
UD_Slovenian-SSJ 96080

True UD_Indonesian-GSD 78923 elmo
UD_Spanish-GSD 297701

False UD_English-EWT 175695 elmo
UD_English-GUM 54570

True UD_Czech-CAC 339999 elmo
UD_Czech-PDT 1006136

False UD_Turkish-IMST 36652 elmo
UD_Turkish-PUD 11605

False UD_Swedish-PUD 13439 elmo
UD_Swedish-Talbanken 68885

True UD_French-GSD 275646 elmo
UD_French-ParTUT 20194

True UD_Czech-CLTT 23717 elmo
UD_Czech-PDT 1006136

True UD_Czech-PDT 1006136 elmo
UD_Estonian-EDT 287552

True UD_Catalan-AnCora 375676 elmo
UD_Spanish-AnCora 382495

True UD_Norwegian-Bokmaal 218607 elmo
UD_Norwegian-Nynorsk 213154

False UD_Italian-ISDT 207813 elmo
UD_Italian-ParTUT 39371

True UD_Romanian-Nonstandard 131639 elmo
UD_Romanian-RRT 152321

