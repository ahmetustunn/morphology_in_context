import sys

if len(sys.argv) < 2:
    print('please specify path')
    exit(1)

datas = []
for embs in ['', '.poly', '.fast', '.elmo','.bert']:
    datas.append([])
    for line in open(sys.argv[1] + embs):
        datas[-1].append(line.strip())

outFile = open(sys.argv[1] + '.all', 'w')
for i in range(len(datas[0])):
    if len(datas[0][i]) < 2 or datas[0][i][0] == '#':
        outFile.write(datas[0][i] + '\n')
    else:
        embs = []
        for dataIdx in range(len(datas)):
            tok = datas[dataIdx][i].split('\t')
            newEmbs = tok[-1][4:].split(',')
            if len(newEmbs) < 3: 
                continue
            embs += newEmbs
        tok = datas[0][i].split('\t')
        outFile.write('\t'.join(tok[:-1] + ['emb=' + ','.join(embs)]) + '\n')
outFile.close()

