import matplotlib.pyplot as plt
import matplotlib as mpl
from copy import deepcopy
import os

plt.style.use('scripts/rob.mplstyle')
colors = plt.rcParams["axes.prop_cycle"].by_key()["color"] 
colors = colors + colors
langs = ['DE', 'IT', 'NL', 'FR', 'PT',] #no ES for now
settings = ['fbfb', 'twfb', 'twtw', 'fbtw']

def setTicks(ax, labels, rotation = 0):
    ticks = []
    for i in range (len(labels)):
        ticks.append(i + .5)

    ax.xaxis.set_major_locator(mpl.ticker.LinearLocator(len(labels)+1))
    ax.xaxis.set_minor_locator(mpl.ticker.FixedLocator(ticks))

    ax.xaxis.set_major_formatter(mpl.ticker.NullFormatter())
    ax.xaxis.set_minor_formatter(mpl.ticker.FixedFormatter(labels))

    for tick in ax.xaxis.get_minor_ticks():
        tick.tick1line.set_markersize(0)
        tick.tick2line.set_markersize(0)
        tick.label1.set_horizontalalignment('right')
        tick.label1.set_rotation(rotation)

def drawGraph(ax,allScores, subDivision, mainDivision, xlabel, ylabel, ylim, location='best'):
    if len(allScores) != len(mainDivision):
        print("error, different size of scores and main labels:", len(allScores), len(mainDivision))
    if len(allScores[0]) != len(subDivision):
        print("error, different size of scores and sub labels:", len(allScores), len(mainDivision))
    print(mainDivision)

    bar_width = 1/ (len(subDivision) + 1)

    for subIdx in range(len(subDivision)):
        index = []
        scores = []
        for mainIdx in range(len(mainDivision)):
            index.append(mainIdx + (subIdx + 1) * bar_width)
            scores.append(allScores[mainIdx][subIdx])

        print(subDivision[subIdx], scores)
        ax.bar(index, scores, bar_width, label=subDivision[subIdx], color=colors[subIdx])
    print()

    setTicks(ax, mainDivision, 45)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim((0,len(mainDivision)))
    ax.set_ylim(ylim)

    leg = ax.legend(loc=location)
    leg.get_frame().set_linewidth(1.5)

def getTrainDevTest(path):
    train = dev = test = ''
    for conlFile in os.listdir(path):
        if conlFile.endswith('train.conllu'):
            train = conlFile
        if conlFile.endswith('dev.conllu'):
            dev = conlFile
        if conlFile.endswith('test.conllu'):
            test = conlFile
    return path+'/'+train, path+'/'+dev, path+'/'+test

def getTrainDev(path):
    return getTrainDevTest(path)[:2]

def getLang(dataset):
    trans = {'kmr':'ku', 'kpv':'kv', 'sme':'se', 'yue':'zhc', }
    for conlFile in os.listdir('data/' + dataset):
        if conlFile.endswith('.conllu'):
            lang = conlFile.split('_')[0]
            if lang == 'no':
                if 'nynorsk' in conlFile:
                    return 'nn'
                else:
                    return 'no'
            if lang in trans:
                lang = trans[lang]
            return lang
    return ''

def getDatasetName(dataset):
    for conlFile in os.listdir('data/' + dataset):
        if conlFile.endswith('.conllu'):
            return conlFile.split('-')[0]
    return ''

def getScores(scoreFile):
    if not os.path.exists(scoreFile):
        return [0.0, 0.0, 0.0, 0.0]
    scores = [0.0, 0.0, 0.0, 0.0]
    for line in open(scoreFile):
        if line.startswith('Lemma Acc'):
            scores[0] = float(line.strip().split(' ')[-1])
        if line.startswith('Lemma Lev'):
            scores[1] = float(line.strip().split(' ')[-1])
        if line.startswith('Morph Acc'):
            scores[2] = float(line.strip().split(' ')[-1])
        if line.startswith('Morph F1'):
            scores[3] = float(line.strip().split(' ')[-1])
    return scores

def getOverlap(set1, set2):
    return len(set1.intersection(set2)) / len(set1)

def findCouples():
    couples = {}
    data = {}
    for dataset in os.listdir('data'):
        if not dataset.startswith('UD'):
            continue
        words = set()
        size = 0
        train, dev = getTrainDev('data/' + dataset)
        for line in open(train):
            tok = line.split('\t')
            if len(tok) < 2 or line[0] == '#':
                continue
            if tok[5] == 'NUM' or tok[5] == '_':
                continue
            words.add(tok[1])
            size += 1
        data[dataset] = words

    for rowIdx, srcDataset in enumerate(sorted(data)):#[:50]:
        candidates = {}
        for tgtDataset in data:
            overlap = getOverlap(data[srcDataset], data[tgtDataset])
            if overlap != 0.0 and overlap != 1.0:
                candidates[tgtDataset] = overlap
        if len(candidates) == 0:
            tgtDataset = None
        else:
            tgtDataset, overlap = sorted(candidates.items(), key=lambda kv: kv[1], reverse=True)[0]
        couples[srcDataset] = tgtDataset
    return couples

def  getCouple(dataset):
    global couples
    if len(couples) == 0:
        couples = findCouples()
    if dataset in couples:
        return couples[dataset]
    else:
        print('error', dataset, ' not found', len(couples))
        return ''

def evaluate(gold, pred):
    cmd = 'python3 src/eval.py ' + gold + ' ' + pred + ' > tmp'
    os.system(cmd)
    scores = getScores('tmp')
    #os.remove('tmp')
    return scores


couples = {}

# find the best performing system with treebank embeddings, return the score and the path
def getTreeScores(dataset, folder):
    global couples
    datasetName = getDatasetName(dataset)
    if len(couples) == 0:
        couples = findCouples()

    cands = []
    srcs = []
    scoreFile1 = folder + '/' + dataset + '--' + str(couples[dataset]) + '/' + datasetName + '.score'
    scoreFile2 = folder + '/' + str(couples[dataset]) + '--' + dataset + '/' + datasetName + '.score'
    if os.path.exists(scoreFile1):
        score = getScores(scoreFile1)
    elif os.path.exists(scoreFile2):
        score = getScores(scoreFile2)
    else:
        print('NOT FOUND: ', dataset, str(couples[dataset]), folder)
        #print(scoreFile1)
        #print(scoreFile2)
        #print()
        score = [0.0, 0.0, 0.0, 0.0]
    return score, couples[dataset]

# get the results of test predictions, return the score and the path
def getTreeScoresTest(dataset, folder):
    global couples
    datasetName = getDatasetName(dataset)
    if len(couples) == 0:
        couples = findCouples()

    cands = []
    srcs = []
    score = [0.0, 0.0, 0.0, 0.0]
    for emb in ['.elmo', '.poly', '.fast', '']:
        scoreFile1 = folder + '/' + dataset + '--' + str(couples[dataset]) + '/' + datasetName + '-um-covered-test.conllu' + emb + '.treebank.output'
        scoreFile2 = folder + '/' + str(couples[dataset]) + '--' + dataset + '/' + datasetName + '-um-covered-test.conllu' + emb + '.treebank.output'
        if os.path.exists(scoreFile1):
            score = evaluate(scoreFile1, 'data/' + dataset + '/' + datasetName + '-um-test.conllu')
            break
        elif os.path.exists(scoreFile2):
            score = evaluate(scoreFile2, 'data/' + dataset + '/' + datasetName + '-um-test.conllu')
            break
    if score[0] == 0.0:
        print("NOT FOUND: " + dataset + '\t' + folder)
    return score, couples[dataset]

# Get the best performing embedding type (also ordered)
def getEmbType(dataset):
    for conlFile in os.listdir('data/' + dataset):
        if 'elmo' in conlFile:
            return 'elmo'
    for conlFile in os.listdir('data/' + dataset):
        if 'poly' in conlFile:
            return 'poly'
    for conlFile in os.listdir('data/' + dataset):
        if 'fast' in conlFile:
            return 'fast'
    return ''


tuneSets = ['UD_English-EWT','UD_English-PUD','UD_Turkish-IMST','UD_Turkish-PUD', 'UD_Chinese-CFL', 'UD_Chinese-GSD', 'UD_Finnish-PUD', 'UD_Finnish-FTB']

def makeTable(columns, data, prec, allignment, ref, cap, bold, extra, oppOrdering, large=False):
    length = len(columns)

    boldGraph = deepcopy(data)
    if bold == 'hor':
        for i in range(len(data)):
            highest = 0.0 if oppOrdering[j] == False else 999
            highestIdx = 0
            for j in range(length):
                if type(data[i][j]) != float:
                    continue
                highest = max(highest, data[i][j]) if oppOrdering[j] == False else min(highest, data[i][j])
            for j in range(length):
                boldGraph[i][j] = (data[i][j] == highest)

    if bold == 'vert':
        for j in range(length):
            highest = 0.0 if oppOrdering[j] == False else 999
            highestIdx = 0
            for i in range(len(data)):
                if type(data[i][j]) != float:
                    continue
                highest = max(highest, data[i][j]) if oppOrdering[j] == False else min(highest, data[i][j])
            for i in range(len(data)):
                boldGraph[i][j] = (data[i][j] == highest)

    for i in range(len(data)):
        if len(data[i]) != length:
            print("error, table rows do not have same length")
        for j in range(length):
            if type(data[i][j]) == float:
                if boldGraph[i][j] == True:
                    data[i][j] = '\\textbf{' + '{:.2f}'.format(data[i][j]) + '}'
                else:
                    data[i][j] = '{:.2f}'.format(data[i][j])
    printTable(columns, data, allignment, ref, cap, extra, large)

def printTable(columns, data, allignment, ref, cap, extra, large=False):
    length = len(columns)
    
    # get max width for each column
    maxSizes = []
    for j in range(length):
        maxSizes.append(len(columns[j]))
    for i in range(len(data)):
        if len(data[i]) != length:
            print("error, table rows do not have same length")
        for j in range(length):
            if len(data[i][j]) > maxSizes[j]:
                maxSizes[j] = len(data[i][j])

    print()
    asterix = '' if not large else '*'
    print('\\begin{table' + asterix + '}')
    print('    \\centering')
    if large:
        print("""    \\setlength\\tabcolsep{.15cm}
    \\resizebox{\\textwidth}{!}{""")
    print('    \\begin{tabular}{' + allignment + '}')
    print('        \\toprule')
    if extra != '':
        print(extra)
    print('        ', end='')
    for j in range(length):
        print(('& ' if j != 0 else '') + columns[j].ljust(maxSizes[j] + 1), end='')
    print('\\\\')
    print('        \\midrule')
    for i in range(len(data)):
        print('        ', end='')
        for j in range(length):
            #if j % 8 == 0 and j != 0:
            #    print('\n            ', end='')
            print(('& ' if j != 0 else '') + data[i][j].ljust(maxSizes[j] + 1), end='')
        print('\\\\')
    
    print('        \\bottomrule')
    print('    \\end{tabular}')
    if large == True:
        print('    }')
    print('    \\caption{' + cap + '}')
    print('    \\label{' + ref + '}')
    print('\\end{table' + asterix + '}')
    print()


