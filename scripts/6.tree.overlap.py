import sys
import os

def getData(dataset, split):
    ending = 'conllu'
    udDir = os.path.join('data', dataset)
    for file in os.listdir(udDir):
        if split in file and file.endswith(ending):
            return os.path.join(udDir, file)
    return ''

data = {}
dataSizes = {}
for dataset in os.listdir('data'):
    if not dataset.startswith('UD'):
        continue
    words = set()
    size = 0
    for line in open(getData(dataset, 'train')):
        tok = line.split('\t')
        if len(tok) < 2 or line[0] == '#':
            continue
        if tok[5] == 'NUM' or tok[5] == '_':
            continue
        words.add(tok[1]) 
        size += 1
    data[dataset] = words
    dataSizes[dataset] = size

def getOverlap(set1, set2):
    return len(set1.intersection(set2)) / len(set1)

def getEmbs(dataset):
    embs = set()
    for conlFile in os.listdir('data/' + dataset):
        for emb in ['elmo', 'fast', 'poly']:
            if conlFile.endswith(emb):
                embs.add(emb)
    return embs

pairs = set()
for srcDataset, size in sorted(dataSizes.items(), key=lambda kv: kv[1]):#[:50]:
    candidates = {}
    for tgtDataset in data:
        overlap = getOverlap(data[srcDataset], data[tgtDataset])
        if overlap != 0.0 and overlap != 1.0:
            candidates[tgtDataset] = overlap
    if len(candidates) == 0:
        #print(srcDataset, size, 'none')
        continue
    tgtDataset, overlap = sorted(candidates.items(), key=lambda kv: kv[1], reverse=True)[0]
    pair = sorted([srcDataset, tgtDataset])
    pairs.add((pair[0], pair[1]))

for pair in pairs:
    srcDataset = pair[0]
    tgtDataset = pair[1]
    srcEmbs = getEmbs(srcDataset)
    tgtEmbs = getEmbs(tgtDataset)
    if 'elmo' in srcEmbs and 'elmo' in tgtEmbs:
        embType = 'elmo'
    elif 'poly' in srcEmbs and 'poly' in tgtEmbs:
        embType = 'poly'
    elif 'fast' in srcEmbs and 'fast' in tgtEmbs:
        embType = 'fast'
    else:
        embType = 'none'
    large = dataSizes[srcDataset] + dataSizes[tgtDataset] > 250000
    print(large, srcDataset, dataSizes[srcDataset], embType)
    print(tgtDataset, dataSizes[tgtDataset])#, overlap)
    print()

#for srcDataset in sorted(data):
#    for tgtDataset in sorted(data):
#        overlap = getOverlap(data[srcDataset], data[tgtDataset])
        #if overlap > 0.2 and overlap < 0.4 and overlap != 1.0:
        #print(srcDataset, tgtDataset, "{0:.2f}".format(overlap * 100))
    

