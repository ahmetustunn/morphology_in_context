import os
import subprocess

def getTrainDevTest(path):
    train =''
    dev =''
    test=''
    for conlFile in os.listdir(path):
        conlFile = conlFile.strip()
        if 'dev.' in conlFile and conlFile.endswith('conllu'):
            dev = conlFile
        if 'train.' in conlFile and conlFile.endswith('conllu'):
            train = conlFile
        if 'test.' in conlFile and conlFile.endswith('conllu'):
            test = conlFile

    return path+'/'+train, path+'/'+dev, path + '/' + test

#same: buryat komi gothic, bambara
#not found: coptic (cop), akkadian(akk), naija (pcm),  
trans = {'kmr':'ku', 'kpv':'kv', 'sme':'se', 'yue':'zhc', }

def wc(textPath):
    if not os.path.exists(textPath):
        return 0
    return int(subprocess.check_output("/usr/bin/wc -l " + textPath, shell=True).split()[0])
    #return len(open(textPath).readlines())

for dataset in os.listdir('data'):
    langCode = ''
    for conlFile in os.listdir('data/' + dataset):
        langCode = conlFile.split('_')[0]
        if langCode == 'no':
            if 'ynorsk' in conlFile.lower():
                langCode = 'non'
            else:
                langCode = 'nob'
    if langCode in trans:
        langCode = trans[langCode]
    if os.path.exists('ELMoForManyLangs/' + langCode):
        for conlFile in getTrainDevTest('data/' + dataset):
            if wc(conlFile) == wc(conlFile + '.elmo'):
                continue
            #print(wc(conlFile), wc(conlFile + '.elmo'), conlFile)
            cmd = 'cd ELMoForManyLangs'
            cmd += ' && python3 3.elmo.py ' + langCode + ' ../' + conlFile + ' > ../' + conlFile + '.elmo'
            cmd += ' && cd ../'
            print(cmd)
        #print(langCode, 'elmo', dataset)
    elif os.path.exists('data/polyglot/' + langCode + '.pickle'):
        for conlFile in getTrainDevTest('data/' + dataset):
            if wc(conlFile) == wc(conlFile + '.poly'):
                continue
            #print(wc(conlFile), wc(conlFile + '.elmo'), conlFile)
            cmd = 'python3 scripts/1.polyglot.py data/polyglot/' + langCode + '.pickle ' + conlFile
            print(cmd)
        #print(langCode, 'poly', dataset)
    elif os.path.exists('data/fasttext/' + langCode):
        for conlFile in getTrainDevTest('data/' + dataset):
            if wc(conlFile) == wc(conlFile + '.fast'):
                continue
            #print(wc(conlFile), wc(conlFile + '.elmo'), conlFile)
            cmd = 'python3 scripts/2.fast.py data/fasttext/' + langCode + ' ' + conlFile
            print(cmd)
        #print(langCode, 'fast', dataset)
    else:
        pass
        #print(langCode, 'none', dataset)

