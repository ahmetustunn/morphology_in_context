import os

def getDatasets(dataset, embs):
    train = dev = test = ""
    end = embs
    if embs == '':
        end == 'conllu'
    for conlFile in os.listdir('data/' + dataset):
        if 'train' in conlFile and conlFile.endswith(end):
            train = conlFile
        if 'dev' in conlFile and conlFile.endswith(end):
            dev = conlFile
        if 'test' in conlFile and conlFile.endswith(end):
            test = conlFile
    return os.path.join('/data/p270396/projects/morphology_in_context/data', dataset, train), os.path.join('/data/p270396/projects/morphology_in_context/data', dataset, dev), os.path.join('/data/p270396/projects/morphology_in_context/data', dataset,test)

def getEmbType(dataset):
    for conlFile in os.listdir('data/' + dataset):
        if 'elmo' in conlFile:
            return 'elmo'
    for conlFile in os.listdir('data/' + dataset):
        if 'poly' in conlFile:
            return 'poly'
    for conlFile in os.listdir('data/' + dataset):
        if 'fast' in conlFile:
            return 'fast'
    return ''

sizes = {'poly':64, 'fast':300, 'elmo':1024, 'bert':768}
for dataset in os.listdir('data'):
    embType = getEmbType(dataset)
    train, dev, test = getDatasets(dataset, embType)
    #if os.path.isfile('models-test-emb/' + dataset + '/score'):
    #    continue
    cmd = 'python3 src/tagger.py --model models-test-emb/' + dataset 
    cmd += ' --train ' + train
    cmd += ' --dev ' + dev
    cmd += ' --test ' + test
    cmd += ' --train_size 250000' 
    if embType != '':
        cmd += ' --enc_wordform_type pretrained'
        cmd += ' --enc_wordform_emb_size ' + str(sizes[embType])
    print(cmd)


